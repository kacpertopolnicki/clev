(* ::Package:: *)

BeginPackage["Clev`"];


dataIndex::usage = "dataIndex[index] 

The string:
   index
is a unique pointer to data. The data is
in the form of a tuple and is located in 
the:
   <manifest directory>/output/index
directory where:
   <manifest directory>
contains the manifest file and is set in:
   initializeClev[...].";
dataBottom::usage = "dataBottom[index]

Indicates that the data pointed to by:
   index
contains an error.";
clearData::usage = "clearData[]

Clears all data and resets the system.";
initializeClev::usage = "initializeClev[toolPath , manifestDirectory]

Initialize the system. The manifest file is inside the:
   manifestDirectory
directory. The path to the:
   runclevtool
script is in:
   toolPath.

Options[initializeClev]={\"Machine\" -> All , \"Queue\"\[Rule] False}
   \"Machine\" - machine to run the jobs on, \"this_machine\" means the local machine
   \"Queue\" - The value True means that the jobs will be queued
";
rawType::usage = "rawType[type][data , option1->value1, option2->value2, ...]

Creates a new raw type from:
   data
returns:
   dataIndex[index]
where
   index
points to the newly created data. The string
   type
is the type associated with 
   data.

The options:
   option1->value1
   option2->value2
   ...
are passed to
   OpenWrite
and the resulting stream is used to write the data.
If:
   Head[data] === ByteArray
then additionally
   BinaryFormat -> True
is passed to:
   OpernWrite.
";
runLoop::usage = "runLoop[]

Runs the manin loop until all jobs are finished.";
getStatus::usage = "getStatus[]

Returns a list:
   {\"submitted\"->sub , \"finished\"->fin , \"bottom\"->bot , \"waiting\"\[Rule]wtn}
where 
   sub
is the number of jobs submitted to the system, 
   fin
is the number of finished jobs,
   bot
is the number of outputs with errors, and
   waiting
is the number of jobs waiting for the availability of their inputs.";
allJobs::usage = "Association list with all the jobs and their status.";
funApp::usage = "funApp[function][index1 , index2 , ...]

Applies the function:
   function
to a tuple created from:
   (index1 , index2 , ...)
Returns
   dataIndex[index]
where
   index
points to data that was created as a result of the function application.";
binApp::usage = "binApp[function][arg1 , arg2 , ...]

Applies binary function that commutes:
   funApp[function][arg1 , arg2] = funApp[function][arg2 , arg1]
to many arguments. The number of arguments has to be more then 2. 
For example:
   binApp[function][arg3 , arg2 , arg1]
Evaluates to:
   funApp[function][arg3 , funApp[function][arg2 , arg1]]";


Begin["`Private`"];


runClevToolPath = "";
manifestDirectory = "";
outputDirectory = "";
allJobs = Association[];


binApp[function_][A___  , B_ , C_] :=
Module[{result},
	result = funApp[function][B , C];
	(*Print[{function , result , {B , C}}];*)
	binApp[function][A , result]
];
binApp[function_][dataIndex[a_] , dataIndex[b_]] :=
Module[{result},
	result = funApp[function][dataIndex[a] , dataIndex[b]];
	(*Print[{function , result , {dataIndex[a] , dataIndex[b]}}];*)
	result
];


machine = All;
queueJobs = False;
Options[initializeClev]={"Machine" -> All , "Queue"-> False};
initializeClev[toolPath_ , manifestDir_ , OptionsPattern[]]:=
Module[{},
	runClevToolPath = toolPath;
	manifestDirectory = manifestDir;
	outputDirectory = FileNameJoin[{manifestDirectory , "output"}];
	machine = OptionValue["Machine"];
	queueJobs = OptionValue["Queue"];
	If[!FileExistsQ[FileNameJoin[{manifestDir , "info"}]],
		Throw["In -initializeClev-, no -info- file found in manifest directory. Try running -runclevtool check_manifest-."];
	];
];


clearData[]:=Module[{command},
	command = runClevToolPath<>" clear_data";
	SetDirectory[manifestDirectory];
	Run[command];
	allJobs = Association[];
	runClevToolPath = "";
	manifestDirectory = "";
	outputDirectory = "";
];


getStatus[]:=Module[{sub , fin , bot , wtn},
	sub = Dimensions[Keys[Select[allJobs , (# === {"submitted"})&]]][[1]];
	fin = Dimensions[Keys[Select[allJobs , (#[[1]] === "finished")&]]][[1]];
	bot = Dimensions[Keys[Select[allJobs , (#[[1]] === "bottom")&]]][[1]];
	wtn = Dimensions[Keys[Select[allJobs , (#[[1]] === "waiting")&]]][[1]];
	{"submitted"->sub , "finished"->fin , "bottom"->bot , "waiting"->wtn}
];


runLoop[]:=
Module[{submitted, waitingfor , outputs , waitingkeys , finished , ii , bottom , continue , waitingjobs , exitcode , command , index,errormessage},
	continue = Not[queueJobs];
	While[continue,
		(*Try submitting waiting jobs.*)
		waitingjobs=Keys[Select[allJobs , (#[[1]] === "waiting")&]];
		For[ii = 1 , ii <= Dimensions[waitingjobs][[1]] , ii++,
			index = waitingjobs[[ii]];
			SetDirectory[manifestDirectory];
			command = allJobs[index][[2]];
			exitcode = RunProcess[command , "ExitCode"];
			If[exitcode === 0,
				AppendTo[allJobs , index->{"submitted"}];,
				If[exitcode=== 10,
					AppendTo[allJobs , index->{"waiting" , command}];,
					If[FileExistsQ[FileNameJoin[{manifestDirectory , "log"}]],
						Print["LOG FILE:"];
						errormessage =Import[FileNameJoin[{manifestDirectory , "log"}] , "Text"];
						Print[errormessage];
					];
					Throw["Error running runclevtool create_function_aplication."];
				];
			];
		];
		waitingfor = Select[allJobs , (# === {"submitted"})&];
		waitingkeys = Keys[waitingfor];
		SetDirectory[outputDirectory];
		outputs = FileNames[];
		finished = Intersection[waitingkeys,outputs];
		For[ii = 1 , ii <= Dimensions[finished][[1]] , ii++,
			bottom = StringTrim[Import[FileNameJoin[{outputDirectory , finished[[ii]] , "bottom"}] , "Text"]];
			If[bottom === "",
				allJobs[finished[[ii]]] = {"finished" , FileNameJoin[{outputDirectory , finished[[ii]]}]};
				,
				allJobs[finished[[ii]]] = {"bottom" , bottom};
			];
		];
		continue = Total[{"submitted" , "waiting"}/.getStatus[]] > 0;
	];
	If[queueJobs,
		SetDirectory[outputDirectory];
		continue = True;
		While[continue,
			outputs = FileNames[];
			submitted = Keys[allJobs];
			If[Dimensions[Complement[submitted , outputs]][[1]] == 0,
				continue = False];
			Pause[0.5];
		];
		finished = Keys[allJobs];
		For[ii = 1 , ii <= Dimensions[finished][[1]] , ii++,
			bottom = StringTrim[Import[FileNameJoin[{outputDirectory , finished[[ii]] , "bottom"}] , "Text"]];
			If[bottom === "",
				allJobs[finished[[ii]]] = {"finished" , FileNameJoin[{outputDirectory , finished[[ii]]}]};
				,
				allJobs[finished[[ii]]] = {"bottom" , bottom};
			];
		];
	];
];


Options[rawType] = Options[OpenWrite];
rawType[type_][data_ , opt:OptionsPattern[]]:=
Module[{command , inputfile , out , exitcode , index , errormessage},
	index = CreateUUID[];
	If[Head[type] === String,
		inputfile = CreateFile[];
		If[!Head[data] === ByteArray,
			out = OpenWrite[inputfile , opt];
			Write[out , data];
			Close[out];,
			out = OpenWrite[inputfile , opt , BinaryFormat->True];
			BinaryWrite[out , data];
			Close[out];
		];
		SetDirectory[manifestDirectory];
		command = "cat "<>inputfile<>" | "runClevToolPath<>" create_raw_type "<>index<>" "<>type;
		exitcode = Run[command];
		DeleteFile[inputfile];
		If[exitcode === 0,
			AppendTo[allJobs , index->{"submitted"}];
			dataIndex[index],
			If[FileExistsQ[FileNameJoin[{manifestDirectory , "log"}]],
				Print["LOG FILE:"];
				errormessage =Import[FileNameJoin[{manifestDirectory , "log"}] , "Text"];
				Print[errormessage];
			];
			Throw["Error running runclevtool create_raw_type."];
		]
		,
		Throw["Expecting string as the type in -rawType-."];
	]
];
funApp[function_][arguments___dataIndex]:=
Module[{index , argindexes , command , exitcode , errormessage},
	index = CreateUUID[];
	If[Head[function] === String,
		SetDirectory[manifestDirectory];
		argindexes = #[[1]]&/@{arguments};
		(*Print[machine];*)
		(*Print[machine === All];*)
		If[machine === All,
			command = Flatten[{runClevToolPath,"create_function_application",function,index,"-a",argindexes}];
		,
			command = Flatten[{runClevToolPath,"create_function_application",function,index,"-c",machine,"-a",argindexes}];
		];
		If[queueJobs,
			AppendTo[command , "-q"];
		];
		(*Print[command];*)
		exitcode = RunProcess[command , "ExitCode"];
		If[exitcode === 0,
			AppendTo[allJobs , index->{"submitted"}];
			dataIndex[index],
			If[exitcode=== 10,
				AppendTo[allJobs , index->{"waiting" , command}];
				dataIndex[index],
				If[FileExistsQ[FileNameJoin[{manifestDirectory , "log"}]],
					Print["LOG FILE:"];
					errormessage =Import[FileNameJoin[{manifestDirectory , "log"}] , "Text"];
					Print[errormessage];
				];
				Throw["Error running runclevtool create_function_aplication."]
			]
		]
		,
		Throw["Expecting string as the function name in -funApp-."];
	]
];


End[];


EndPackage[];
