all : c_tools check_dependencies_out

check_dependencies_out:
	check_dependencies

c_tools:
	make -C c/tools

clean: 
	make -C c/tools clean
	rm -f check_dependencies_out

.PHONY : c_tools clean
