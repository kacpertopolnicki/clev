import os
import argparse
import ConfigParser
import subprocess
import sys

from partype import parsemanifest

class clevData(object):
    """ Various data for clev.

    Attributes:

        manifest_directory (str) :  path to directory containing the 
                                    .manifest file
        manifest_name (str) :       the name of the manifest file, without
                                    the .manifest extension
        manifest_path (str) :       full path to the .manifest file
        has_make (bool) :           indicates if there is a Makefile
        functions (dict) :          dictionary with functions from the manifest 
        function_paths (dict) :     dictionary with functions paths from the
                                    manifest
        typepaths (dict) :          dictionary with types
        ok (book) :                 indicates if the data is properly
                                    initialized
        config (ConfigParser) :     parsed manifest file
    """
   
    quiet = False

    @staticmethod
    def set_quiet():
        """After execution, no output will be 
        printed to stdout.
        """
        clevData.quiet = True

    @staticmethod
    def set_verbose():
        """After execution, verbose output will
        be printed to stdout.
        """
        clevData.quiet = False

    @staticmethod
    def print_message(*message):
        if(not clevData.quiet):
            for m in message:
                sys.stdout.write(str(m))
                sys.stdout.write(" ")
            sys.stdout.write("\n")

    def get_manifest_directory(self):
        """Getter method for:
            self.manifest_directory
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        return self.manifest_directory

    def get_manifest_name(self):
        """Getter method for:
            self.manifest_name
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        return self.manifest_name
    
    def get_manifest_path(self):
        """Getter method for:
            self.manifest_path
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        return self.manifest_path
    
    def get_functions(self):
        """Getter method for:
            self.functions
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        if(None == self.functions):
            raise ValueError('Run -check_functions_types()- to initialize function dict.')
        if(self.functions == None):
            try:
                self.functions = parsemanifest.getfunctions(self.manifest_path)
            except ValueError as error:
                #self.ok = False
                noerr = False
                clevData.print_message("[clevdata] -ERROR- " , error.message.strip())
        return self.functions

    def get_function_paths(self):
        """Getter method for:
            self.function_paths
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        if(None == self.function_paths):
            raise ValueError('Run -check_functions_types()- to initialize function_paths dict.')
        return self.function_paths
    
    def get_type_paths(self):
        """Getter method for:
            self.typepaths
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        if(None == self.typepaths):
            raise ValueError('Run -check_functions_types()- to initialize typepaths dict.')
        return self.typepaths

    def get_has_make(self):
        """Getter method for:
            self.has_make
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        if(None == self.has_make):
            raise ValueError('Run -run_make()- or -run_make_clean()- to initialize has_make.')
        return self.has_make
 
    def get_config(self):
        """Getter method for:
            self.config
        Raises:
            ValueError : if the data is not initialized properly
        """
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
        return self.config  

    def run_make(self):
        """ Run:
                make
            in manifest directory.

            Returns:
                True : all ok
                False : errors encountered
        """
        
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')

        noerr = True

        #########################################
        # Check if there is a                   #
        #    Makefile                           #
        # If it exists, run it.                 #
        #########################################

        self.has_make = False
        try:
            os.stat(os.path.join(self.manifest_directory , "Makefile"))
            self.has_make = True
        except:
            try:
                os.stat(os.path.join(self.manifest_directory , "makefile"))
                self.has_make = True
            except:
                clevData.print_message("[clevdata] NO MAKEFILE FOUND")
     
        if(self.has_make):
            clevData.print_message("[clevdata] RUNNING MAKE")
            #subprocess.call(["make" , "-C" , self.manifest_directory , "clean"])
            rc = subprocess.call(["make" , "-C" , self.manifest_directory])
            if(rc != 0):
                clevData.print_message("[clevdata] -ERROR- make returned non zero exit code")
                #self.ok = False
                noerr = False
        return noerr


    def run_make_clean(self):
        """ Run:
                make clean
            in manifest directory. 

            Returns:
                True : all ok
                False : errors encountered
        """

        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
       
        noerr = True

        #########################################
        # Check if there is a                   #
        #    Makefile                           #
        # If it exists, run it.                 #
        #########################################

        self.has_make = False
        try:
            os.stat(os.path.join(self.manifest_directory , "Makefile"))
            self.has_make = True
        except:
            try:
                os.stat(os.path.join(self.manifest_directory , "makefile"))
                self.has_make = True
            except:
                clevData.print_message("[clevdata] NO MAKEFILE FOUND")
     
        if(self.has_make):
            clevData.print_message("[clevdata] RUNNING MAKE CLEAN")
            rc = subprocess.call(["make" , "-C" , self.manifest_directory , "clean"])
            #subprocess.call(["make" , "-C" , self.manifest_directory])
            if(rc != 0):
                clevData.print_message("[clevdata] -ERROR- make clean returned non zero exit code")
                #self.ok = False
                noerr = False
        return noerr

    def make_template(self):
        """Checks the availability of functions and types.

            Returns:
                True : all ok
                False : errors encountered
        """
        
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
       
        noerr = True
        
        #########################################
        # Make template for functions.          #
        #########################################
 
        try:
            self.functions = parsemanifest.getfunctions(self.manifest_path)
        except ValueError as error:
            #self.ok = False
            noerr = False
            clevData.print_message("[clevdata] -ERROR- " , error.message.strip())
        
        self.function_paths = {}

        if(noerr):
            for function in self.functions:
                clevData.print_message("[clevdata] MAKING TEMPLATE FOR FUNCTION : " , function)
                function_path = None
                try:
                    function_path = self.config.get("FUNCTION_PATHS" , function)
                except:
                    clevData.print_message("[clevdata] -ERROR- No function path found.")
                    #self.ok = False
                    noerr = False
                if(noerr):
                    self.function_paths.update({function : function_path})
                if(function_path != None):
                    for fun in self.functions[function][0]:
                        function_full_path = os.path.join(self.manifest_directory , self.manifest_name ,
                                function_path , ",".join(fun))
                        try:
                            os.stat(function_full_path)
                        except:
                            print "[clevdata] CREATING: " , function_full_path
                            rc = subprocess.call(["mkdir" , "-p" , os.path.join(self.manifest_directory , self.manifest_name ,
                                    function_path)])
                            if(rc == 0):
                                rc = subprocess.call(["touch" , function_full_path])
                                if(rc != 0):
                                    raise RuntimeError("Could not create file for: " + function_full_path)
                            else:
                                raise RuntimeError("Could not create directories for: " + function_full_path)


        #########################################
        # Make template for types.              #
        #########################################

        self.typepaths = self.config.options("RAW_TYPES")

        for tpe in self.typepaths:
            print "[clevdata] CREATING TEMPLATE FOR TYPE:" , tpe
            type_path = self.config.get("RAW_TYPES" , tpe)
            type_full_path = os.path.join(self.manifest_directory , self.manifest_name ,
                    type_path , tpe)
            try:
                os.stat(type_full_path)
            except:
                print "[clevdata] CREATING: " , type_full_path
                rc = subprocess.call(["mkdir" , "-p" , 
                    os.path.join(
                        self.manifest_directory , 
                        self.manifest_name , 
                        type_path)])
                if(rc == 0):
                    rc = subprocess.call(["touch" , type_full_path])
                    if(rc != 0):
                        raise RuntimeError("Could not create file for: " + type_full_path)
                else:
                    raise RuntimeError("Could not create directories for: " + type_full_path)

        
        return noerr

    def check_functions_types(self):
        """Checks the availability of functions and types.

            Returns:
                True : all ok
                False : errors encountered
        """
        
        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
       
        noerr = True
        
        #########################################
        # Check the availability of functions.  #
        #########################################
 
        try:
            self.functions = parsemanifest.getfunctions(self.manifest_path)
        except ValueError as error:
            #self.ok = False
            noerr = False
            clevData.print_message("[clevdata] -ERROR- " , error.message.strip())
        
        self.function_paths = {}

        if(noerr):
            for function in self.functions:
                clevData.print_message("[clevdata] CHECKING FUNCTION : " , function)
                function_path = None
                try:
                    function_path = self.config.get("FUNCTION_PATHS" , function)
                except:
                    clevData.print_message("[clevdata] -ERROR- No function path found.")
                    #self.ok = False
                    noerr = False
                if(noerr):
                    self.function_paths.update({function : function_path})
                if(function_path != None):
                    for fun in self.functions[function][0]:
                        try:
                            status = os.stat(os.path.join(self.manifest_directory , self.manifest_name ,
                                function_path , ",".join(fun)))
                            if(not os.access(os.path.join(self.manifest_directory , self.manifest_name ,
                                function_path , ",".join(fun)), os.X_OK)):
                                raise RuntimeError("Can not execute.") 
                            clevData.print_message("[clevdata] -OK-    " , ",".join(fun) , "found and can be executed.")
                        except:
                            clevData.print_message("[clevdata] -ERROR- " , ",".join(fun) , "not found or could not be executed.")
                            #self.ok = False
                            noerr = False

        #########################################
        # Check the availability of types.      #
        #########################################

        self.typepaths = self.config.options("RAW_TYPES")

        for tpe in self.typepaths:
            clevData.print_message("[clevdata] CHECKING TYPE : " , tpe)
            type_path = self.config.get("RAW_TYPES" , tpe)
            try:
                status = os.stat(os.path.join(self.manifest_directory , self.manifest_name ,
                    type_path , tpe))
                if(not os.access(os.path.join(self.manifest_directory , self.manifest_name ,
                    type_path , tpe), os.X_OK)):
                    raise RuntimeError("Can not execute.") 
                clevData.print_message("[clevdata] -OK-    " , tpe , "found and can be executed.")
            except:
                clevData.print_message("[clevdata] -ERROR- " , tpe , "not found or could not be executed.")
                #self.ok = False
                noerr = False
        return noerr

    def check_directories(self):
        """Check if necessary directories exist.
        If not, try to create them.

        Returns:
            True : directories exist
            False : directories don't exit
        """

        if(not self.ok):
            raise ValueError('The clevData object was not initialized properly.')
       
        noerr = True
        
        #########################################
        # Check if                              #
        #    output                             #
        #    work                               #
        #    jobs                               #
        #    machine                            #
        # directory exists.                     #
        #########################################

        clevData.print_message("[clevdata] CHECKING FOR NECESSARY DIRECTORIES")

        try:
            os.stat(os.path.join(self.manifest_directory , "output"))
            clevData.print_message("[clevdata] -OK-     output directory found.")
        except:
            clevData.print_message("[clevdata] CREATING OUTPUT DIRECTORY")
            try:
                os.mkdir(os.path.join(self.manifest_directory , "output"))
            except:
                clevData.print_message("[clevdata] -ERROR-  no output directory in manifest directory")
                #self.ok = False
                noerr = False

        try:
            os.stat(os.path.join(self.manifest_directory , "work"))
            clevData.print_message("[clevdata] -OK-     work directory found.")
        except:
            clevData.print_message("[clevdata] CREATING WORK DIRECTORY")
            try:
                os.mkdir(os.path.join(self.manifest_directory , "work"))
            except:
                clevData.print_message("[clevdata] -ERROR-  no work directory in manifest directory")
                #self.ok = False
                noerr = False

        try:
            os.stat(os.path.join(self.manifest_directory , "jobs"))
            clevData.print_message("[clevdata] -OK-     jobs directory found.")
        except:
            clevData.print_message("[clevdata] CREATING JOBS DIRECTORY")
            try:
                os.mkdir(os.path.join(self.manifest_directory , "jobs"))
            except:
                clevData.print_message("[clevdata] -ERROR-  no jobs directory in manifest directory")
                #self.ok = False
                noerr = False

        try:
            os.stat(os.path.join(self.manifest_directory , "machines"))
            clevData.print_message("[clevdata] -OK-     machines directory found.")
        except:
            clevData.print_message("[clevdata] CREATING MACHINES DIRECTORY")
            try:
                os.mkdir(os.path.join(self.manifest_directory , "machines"))
            except:
                clevData.print_message("[clevdata] -ERROR-  no machines directory in manifest directory")
                #self.ok = False
                noerr = False
        return noerr

    def check_all(self):
        #########################################
        # Indicates if the data was initialized #
        # properly.                             #
        #########################################

        self.ok = True
        
        #########################################
        # Try to run make clean.                #
        #########################################
        
        self.ok = self.ok and self.run_make_clean()
        
        #########################################
        # Try to run make.                      #
        #########################################
        
        self.ok = self.ok and self.run_make()
       
        #########################################
        # Check the availability of functions   #
        # and types.                            #
        #########################################
        
        self.ok = self.ok and self.check_functions_types()

        #########################################
        # Check if                              #
        #    output                             #
        #    work                               #
        #    jobs                               #
        #    machine                            #
        # directory exists.                     #
        #########################################

        self.ok = self.ok and self.check_directories()

        if(self.ok):
            clevData.print_message("[clevdata] NO ERRORS FOUND IN CHECK_ALL")

    def __init__(self , manifestdir):
        """ Initialize a clevData object.

        Arguments:
            manifestdir (str) :     path to directory containing the .manifest
                                    file
        """
       
        self.manifest_directory = manifestdir ##

        #########################################
        # Determining locations related to the  #
        # manifest.                             #
        #########################################

        manif = []
        for f in os.listdir(manifestdir):
            if(f.split('.')[-1] == 'manifest'):
                manif.append(f)
        if(len(manif) != 1):
            raise ValueError('Expecting one manifest file in make_template directory.')
        self.manifest_name = manif[0].replace('.manifest' , '') ##

        self.manifest_path = os.path.join(self.manifest_directory , manif[0]) ##
        
        #########################################
        # Parsing manifest file to:             #
        #                                       #
        #    config                             #
        #########################################
        
        self.config = ConfigParser.ConfigParser()
        self.config.optionxform = str
        self.config.read(self.manifest_path)

        #########################################
        # Indicates if the data was initialized #
        # properly.                             #
        #########################################
        
        self.ok = True

        self.functions = None
        self.function_paths = None
        self.typepaths = None
        self.has_make = None
