import ConfigParser
import tempfile
import os
import subprocess
from inspect import currentframe, getframeinfo

import parsealgebraictype

def checkmanifest(path , verbose = False):
    """
    Check if the manifest contains all necessary information.

    Arguments

    path
        isinstace(path , str) == True
        Path to configuration file with manifest

    verbose
        isinstance(verbose , bool) == True
        If this optional argument is set to True additional information
        will be printed to stdout

    Returns

        True - if manifest is ok
        False - if the manifest is incomplete

    """
    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(path)

    # Checking ALGEBRAIC TYPES
    for at in config.options("ALGEBRAIC_TYPES"):
        #print "1>" , at
        #print "2>" , parsealgebraictype.get_elements(config.get("ALGEBRAIC_TYPES" , at))
        for tp in parsealgebraictype.get_elements(config.get("ALGEBRAIC_TYPES" , at)):
            #print "1>>" , tp
            #print "2>>" , config.options("RAW_TYPES")
            if(not tp in config.options("RAW_TYPES")):
                if(verbose):
                    print "Algebraic type \n" + str(at) + "\ncomposed from types not specified in manifest"
                return False
    
    # Checking FUNCTIONS
    for fn in config.options("FUNCTIONS"):
        for tp in parsealgebraictype.get_elements(config.get("FUNCTIONS" , fn)):
            if(not tp in config.options("RAW_TYPES")):
                if(tp in config.options("ALGEBRAIC_TYPES")):
                    pass
                else:
                    if(verbose):
                        print "Function \n" + str(fn) + "\ncomposed from types not specified in manifest" 
                    return False

    # Checking if each FUNCTION has a path
    for fn in config.options("FUNCTIONS"):
        if(not fn in config.options("FUNCTION_PATHS")):
            if(verbose):
                print "No function path specified for " + str(fn)
            return False

    return True

def buildmanifest(conpath , despath , verbose = False):
    """
    Build types and functions from manifest. 
    Create manifest with succesfully built functions and types.

    Arguments

    conpath
        isinstace(conpath , str) == True
        Path to configuration file with manifest

    despath
        isinstace(conpath , str) == True
        Destination directory path
    """
    if(not checkmanifest(conpath , verbose = verbose)):
        raise ValueError("Manifest " + str(path) + " is incomplete")

    rawpaths = getrawtypes(conpath)
    functionpaths = getfunctionpaths(conpath)
    functionsignatures = getfunctions(conpath)

    #if(not os.path.exists(despath)):
    #    raise ValueError("Destination path " + str(despath) + " does not exist")

    print "\nMAKING TYPES"
    for rt in rawpaths:
        pth = os.path.join(despath , rawpaths[rt])
        spth = os.path.join(os.path.dirname(conpath) , rawpaths[rt] , "*")
        print "cp -r " , spth , pth
        print "make -D " + pth  + " check_" + str(rt) + "_"
        #os.makedirs(pth)
        #subprocess.check_call(["cp" , "-r" , os.path.dirname(conpath) , pth])

    print "\nMAKING FUNCTIONS"
    for fn in functionpaths:
        pth = os.path.join(despath , functionpaths[fn])
        spth = os.path.join(os.path.dirname(conpath) , functionpaths[fn] , "*")
        print "cp -r " , spth , pth
        for sg in functionsignatures[fn][0]:
            sign = ""
            for s in sg:
                sign = sign + str(s).strip() + "_"
            print "make -D " + pth  + " apply_" + str(fn) + "_" + sign
        #os.makedirs(pth)
        #subprocess.check_call(["cp" , "-r" , os.path.dirname(conpath) , pth])

def getfunctionpaths(path , verbose = False):
    """
    Returns a dictionary with function directory paths.

    Arguments

    path
        isinstance(path , str) == True
        Path to configuration file containing the manifest

    verbose
        isinstance(verbose , bool) == True
        If this argument is set to True then additional information
        will be printed to stdout

    Returns

    Dictionary with the function directories. Each function directory
    schould contain a Makefile that can produce each version of the function.
    """
    if(not checkmanifest(path , verbose = verbose)):
        raise ValueError("Manifest " + str(path) + " is incomplete")

    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(path)

    result = {}
    for fn in config.options("FUNCTION_PATHS"):
        result.update({fn : config.get("FUNCTION_PATHS" , fn)})
    return result

def getfunctions(path , verbose = False):
    """
    Returns a dictionary with the signatures of the function argument and 
    return type expanded. Expanded types contain only the raw types. 

    For example if 
        D = (A | B)
    is an algeraic type composed from raw types and the function
        f
    has the signature
        (D , D) -> D
    with the return type:
        D == (A | B)
    and the argument type:
        (D , D) == ((A | B) , (A | B)) == ((A , A) | (A , B) | (B , A) | (B , B))
    then -getfunctions- will return the dictionary
        {"f" : [["A" , "A"] , ["A" , "B"] , ["B" , "A"] , ["B" , "B"]] , [["A"] , ["B"]]] , ...}
               -------------------------------------------------------   ---------------
                                         ||                                    ||
                                         \/                                    \/
                      ((A , A) | (A , B) | (B , A) | (B , B))               (A | B)

    Arguments

    path
        isinstance(path , str) == True
        Path to configuration file containing the manifest

    verbose
        isinstance(verbose , bool) == True
        If this argument is set to True then additional information
        will be printed to stdout
    
    Returns

        Dictionary with the argument type and rerutn type expanded
        {<function name> : [{<expanded argument type>} , {<expanded return type>}]}
    """

    if(not checkmanifest(path , verbose = verbose)):
        raise ValueError("Manifest " + str(path) + " is incomplete")

    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(path)
    
    result = {}
   
    if(verbose):
        print "[FUNCTIONS]"
    for fn in config.options("FUNCTIONS"):
        replacedfn = config.get("FUNCTIONS" , fn)
        #print "1>>" , fn
        #print "2>>" , replacedfn
        #print "3>>" , parsealgebraictype.get_elements(config.get("FUNCTIONS" , fn))
        for tp in parsealgebraictype.get_elements(config.get("FUNCTIONS" , fn)):
            if(not tp in config.options("RAW_TYPES")):
                if(tp in config.options("ALGEBRAIC_TYPES")):
                    for at in config.options("ALGEBRAIC_TYPES"):
                        #print "1>>>" , tp , at  
                        #print "2>>>" , replacedfn
                        replacedfn = parsealgebraictype.replace_element(replacedfn , tp , config.get("ALGEBRAIC_TYPES" , tp))
                        #print "3>>>" , replacedfn
                    pass
                else:
                    raise ValueError("Function \n" + str(fn) + "\ncomposed from types not specified in manifest") 
        signature = replacedfn.split("->")
        if(len(signature) != 2):
            raise ValueError("Expecting a function argument and a function return type in manifest for function " + str(fn) + "\n")
        if(signature[0].strip() == ""):
            raise ValueError("Argument type can not be an empty string for function " + str(fn) + "\n")
        if(signature[1].strip() == ""):
            raise ValueError("Return type can not be an empty string for function " + str(fn) + "\n")
        signature_expanded = map(parsealgebraictype.parse , signature)
        if(verbose):
            print fn , "=" , config.get("FUNCTIONS" , fn) , "==" , replacedfn , "==" , signature_expanded[0] , "->" , signature_expanded[1]
        result.update({fn : [signature_expanded[0] , signature_expanded[1]]})
    return result

def getrawtypes(path , verbose = False):
    """
    Returns a dictionary with the raw types.
    If 
        A
    is a raw type from the manifest in the configuration file
    then -getrawtypes- will return the dictionary:
        {"A" : <path to firectory with the type>}

    Arguments

    path
        isinstance(path , str) == True
        Path to configuration file containing the manifest

    verbose
        isinstance(verbose , bool) == True
        If this argument is set to True then additional information
        will be printed to stdout
    
    Returns

        Dictionary with the argument type and rerutn type expanded
        {<function name> : [[<expanded return type>] , [<expanded argument type>]]}
    """
    
    if(not checkmanifest(path , verbose = verbose)):
        raise ValueError("Manifest " + str(path) + " is incomplete")

    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(path)
    
    result = {}
   
    if(verbose):
       print "[RAW_TYPES]"
    for rt in config.options("RAW_TYPES"):
        result.update({rt : config.get("RAW_TYPES" , rt)})
        if(verbose):
            print rt , "=" , config.get("RAW_TYPES" , rt) 
    return result

def getwlpackages(path , verbose = False):
    """
    Get Wolfram Language packages.

    Arguments

    path
        isinstance(path , str) == True
        Path to configuration file containing manifest

    verbose
        isinstance(verbose , bool) == True
        If this argument is set to True then additional information
        will be printed to stdout
    
    Returns

        List of paths to Wolfram Language packages. All paths are
        relative to the manifest's base directory
        [<path to package 1, eg. "wlpackages/aaaa.wl"> , <path to package 2> , ...]

    """
    if(not checkmanifest(path , verbose = verbose)):
        raise ValueError("Manifest " + str(path) + " is incomplete")

    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(path)
    
    result = []
   
    if(verbose):
       print "[WL_PACKAGES]"
    if(config.has_section("WL_PACKAGES")):
        for wlpg in config.options("WL_PACKAGES"):
            result.append(config.get("WL_PACKAGES" , wlpg))
            if(verbose):
                print wlpg , "=" , config.get("WL_PACKAGES" , wlpg)
    return result

def getlibraries(path , verbose = False):
    """
    Get Wolfram Language packages.

    Arguments

    path
        isinstance(path , str) == True
        Path to configuration file containing manifest

    verbose
        isinstance(verbose , bool) == True
        If this argument is set to True then additional information
        will be printed to stdout
    
    Returns

        List of paths to Wolfram Language packages. All paths are
        relative to the manifest's base directory
        [<path to package 1, eg. "wlpackages/aaaa.wl"> , <path to package 2> , ...]

    """
    if(not checkmanifest(path , verbose = verbose)):
        raise ValueError("Manifest " + str(path) + " is incomplete")

    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(path)
    
    result = {}
   
    if(verbose):
       print "[LIBRARIES]"
    if(config.has_section("LIBRARIES")):
        for lib in config.options("LIBRARIES"):
            result.update({lib : config.get("LIBRARIES" , lib)})
            if(verbose):
                print lib , "=" , config.get("LIBRARIES" , lib)
    return result

def test_manifestparser():

    print "-----------------------------------"
    print "TESTING -parsemanifest- MODULE"
    print "-----------------------------------"
    ma = """
[RAW_TYPES]

A = <path to A>
B = <path to B>
C = <path to C>

[ALGEBRAIC_TYPES]

AB = (A | B)
AC = (A , C)

[FUNCTIONS]

f = (AB , AB) -> AB
g = A -> A

[FUNCTION_PATHS]

f = <path to f>
g = <paht to g>
    """
    with tempfile.NamedTemporaryFile() as tf:
        tf.write(ma)
        tf.flush()
        raw = getrawtypes(tf.name)
        fun = getfunctions(tf.name)
        ok = True
        ok = ok and (raw == {'A' : '<path to A>' , 'B' : '<path to B>' , 'C' : '<path to C>'})
        ok = ok and (fun == {'g' : [{tuple(['A'])} , {tuple(['A'])}] , 'f' :
            [{tuple(['A' , 'A']) , tuple(['A' , 'B']) , tuple(['B' , 'A']) ,
                tuple(['B' , 'B'])} , {tuple(['A']) , tuple(['B'])}]})
        if(ok):
            print "[OK]\tParsing manifest"
        else:
            iframeinfo = getframeinfo(currentframe())
            print "[FAIL] " + str(iframeinfo)

if(__name__ == "__main__"):
    test_manifestparser()
    #buildmanifest("../../temp_manifest" , "/some/fancy/path")

#TODO - add more tests
