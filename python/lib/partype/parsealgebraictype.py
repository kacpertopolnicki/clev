import os
from inspect import currentframe, getframeinfo
import ConfigParser
import re

def expand_string_expression(string):
    """
    Take an algebraic expression in 
        string
    and expand it to sum of tuples.

    Arguments:

        string
            isinstance(string , str) == True
            A string containing the algebraic expression.

    """
    if(not isinstance(string , str)):
        raise ValueError('Expecting a string in expand_string_expression.')
   
    # Remove whitespaces from string:
    result = string.strip().replace(" " , "").replace("\n" , "").replace("\t" , "")
    
    # Check if number of ( and ) is the same:
    bk = 0 # number of open brackets
    for c in result:
        if(c == '('):
            bk = bk + 1
        if(c == ')'):
            bk = bk - 1
    if(bk != 0):
        raise ValueError('The number of brackets in argument to expand_string_expression does not match.')

    #i = 0
    #for c in result:
    #    print str(i) + "\t" + c
    #    i = i + 1

    pos = 0 # position in string
    bk = 0 # number of open brackets
    last_pls = -1 # last +
    next_pls = len(result) # next +
    for c in result:
        if(c == '('):
            bk = bk + 1
        if(c == ')'):
            bk = bk - 1
        if(c == '+'):
            last_pls = pos
            pos1 = pos + 1
            for c1 in result[pos + 1 :]:
                if(c1 == '+'):
                    next_pls = pos1
                pos1 = pos1 + 1
        ###
        if(c == "|"):
            #print "                     | position: " , pos
            # Search for the next , or ) that determine the end of a subexpression.
            # For example: 
            # ...... , a | b , ........
            #                ^ 
            next_cb = None # position of next , or ) in string
            pos1 = pos + 1
            bk1 = bk
            next_ok = True
            for c1 in result[pos1 : ]:
                if(next_ok and (c1 == ',' or c1 == ')')):
                    next_cb = pos1
                    break
                if(c1 == '('):
                    bk1 = bk1 + 1
                if(c1 == ')'):
                    bk1 = bk1 - 1
                #print "                     pos1 , bk1 , bk: " , pos1 , bk1 , bk
                if(bk1 <= bk):
                    next_ok = True
                else:
                    next_ok = False
                pos1 = pos1 + 1
            #print "                     next_cb: " , next_cb
            # Search for the last , or ) that determine the end of a subexpression.
            # For example:
            # ...... , a | b , ........
            #        ^        
            last_cb = None # position of last , or ( in string
            pos1 = pos - 1
            bk1 = bk # bk from the right to the left schould be the same
            last_ok = True
            for c1 in reversed(result[0 : pos1 + 1]):
                if(last_ok and (c1 == ',' or c1 == '(')):
                    last_cb = pos1
                    break
                if(c1 == ')'):
                    bk1 = bk1 + 1
                if(c1 == '('):
                    bk1 = bk1 - 1
                if(bk1 <= bk):
                    last_ok = True
                else:
                    last_ok = False
                pos1 = pos1 - 1
            before = ""
            alt1 = result[: pos]
            if(last_cb != None):
                before = result[0 : last_cb + 1]
                alt1 = result[last_cb + 1 : pos]
            after = ""
            alt2 = result[pos + 1 :]
            if(next_cb != None):
                after = result[next_cb :]
                alt2 = result[pos + 1 : next_cb]
            #print "                     before:" , before
            #print "                     after:" , after
            #print "                     alt1:" , alt1
            #print "                     alt2:" , alt2
            #print "                     last_cb: " , last_cb
            #print "                     pos: " , pos
            #print "                     next_cb: " , next_cb , result[next_cb :]
            #print "                     A:" , before + alt1 + after
            #print "                     B:" , before + alt2 + after
            result = [(before + alt1 + after) , (before + alt2 + after)]
            #result = \
            #        result[0 : last_cb + 1] + result[pos + 1 :] + \
            #        " | " + \
            #        result[0 : last_cb] + result[last_cb : pos] + result[next_cb :] 
            return result
        ###
        pos = pos + 1
    return [result]

def return_alternatives(maxiter , string):
    """
    Return a set of alternative type tuples.

    Arguments:
        
        maxiter
        isinstance(maxiter , int) == True
        Maximum number of iterations. If the number of iterations is exeeded
        an error will be raised.

        string
        isinstance(string , str) == True
        String containing the algebraic expression.

    Returns:
        Set { ... } of tuples ( ... ). Each element of the tuple is a type.
    """
    result = [string]
    it = 1
    while(True):
        if(it >= maxiter):
            raise ValueError('Maximum number of iterations exedded in return_alternatives.')
        oldl = len(result)
        toadd = []
        ind = 0
        for alt in result:
            new = expand_string_expression(alt)
            toadd = toadd + new
            result[ind] = None
            ind = ind + 1
        result = filter(lambda x : x != None , result) 
        result = result + toadd
        if(len(result) == oldl):
            break
        it = it + 1
    result = set(map(lambda x : tuple(x.replace(',' , ' ').replace(')' , ' ').replace('(' , ' ').split()) , result))
    return result


def get_elements(expr):
    """
    Get list of types algebraic expression.

    Arguments

    expr
        isinstance(expr , str) == True
        A string containing the algebraic expression.
        Examples can be found in -test_algebraictypeparser-.

    Returns:

        A list of types in type expression.
        For example
            (A , (B | C)) -> (D | E)
        returns
            [A , B , C , D , E]
    """
    result = []
    for te in re.split('\(|\)|,|\||->' , expr):
        if(te.strip() != ""):
            if(not te.strip() in result):
                result.append(te.strip())
    return result

def replace_element(expr , old , new):
    """
    Replace one type (old) with other type (new)
    in type expression.

    Arguments

    expr
        isinstance(expr , str) == True
        A string containing the algebraic expression.
        Examples can be found in -test_algebraictypeparser-.

    old
        isinstance(old , str) == True
        Old type to be replaced.

    new
        isinstance(new , str) == True
        New replacement type.

    Returns:

        Type expression with the replacement. For example
        replacing
            AB
        with 
            (A | B)
        in
            (AB , A)
        results in 
            ((A | B) , A)

    """
    return re.sub(r'\b' + old.strip() + r'\b' , new.strip() , expr)
    

def parse(expr , debug = False):
    """
    Parse algebraic type expression and expand it.
    
    Arguments:
    
    expr
        isinstance(expr , str) == True
        A string containing the algebraic expression.
        Examples can be found in -test_algebraictypeparser-.
    debug
        isinstace(debug , bool)
        If set to -True- then this function will
        print additional information to stdout.

    Returns:

        A set of tuples. Each sublist represents
        an alternative tuple for the type. For instace
            (A , (B | C))
        is equivalent to
            (A , B) | (A , C)
        and will parse to
            {(A , B) , (A , C)}
        Tuples are flattend out
            (A , (B , C))
        wil parse to
            {(A , B , C)}
    """
    if(debug): print "parse(" , expr , ")"
    return return_alternatives(20 , expr)

def __to_set(expr):
    return set(tuple(row) for row in expr)

def test_algebraictypeparser():
    """
    Run tests for this module.
    """
    print "-----------------------------------"
    print "TESTING -parsealgebraictype- MODULE"
    print "-----------------------------------"
    
    chk = "((aaa , bbb) | (ccc , ddd) | (aaa , bbb))"
    test = parse(chk) == __to_set([["ccc" , "ddd"] , ["aaa" , "bbb"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(aa , bb , cc , ((dd , ee) | ff))"
    test = parse(chk) == __to_set([
                        ["aa" , "bb" , "cc" , "dd" , "ee"],
                        ["aa" , "bb" , "cc" , "ff"]
                        ])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(aa , ((bb)) , cc , (((dd , ee) | ff)))"
    test = parse(chk) == __to_set([
                        ["aa" , "bb" , "cc" , "dd" , "ee"],
                        ["aa" , "bb" , "cc" , "ff"]
                        ])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "((m , n , o) | (a , ((b , (e | (g,h , l))) | ((c|x) , f))))"
    test = parse(chk) == __to_set([
                        ["m" , "n" , "o"],
                        ["a" , "b" , "e"] , 
                        ["a" , "b" , "g" , "h" , "l"],
                        ["a" , "c" , "f"],
                        ["a" , "x" , "f"]
                        ])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(a , ((b , (e | (g,h , l))) | ((c|x) , f)))"
    test = parse(chk) == __to_set([
                        ["a" , "b" , "e"] , 
                        ["a" , "b" , "g" , "h" , "l"],
                        ["a" , "c" , "f"],
                        ["a" , "x" , "f"]
                        ])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(a , ((b , (e | g)) | ((c|x) , f)))"
    test = parse(chk) == __to_set([["a" , "b" , "e"] , ["a" , "b" , "g"] , ["a" , "c" , "f"] , ["a" , "x" , "f"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(a , ((b , (e | g)) | (c , f)))"
    test = parse(chk) == __to_set([["a" , "b" , "e"] , ["a" , "b" , "g"] , ["a" , "c" , "f"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(a , ((b , e) | (c , f)))"
    test = parse(chk) == __to_set([["a" , "b" , "e"] , ["a" , "c" , "f"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(aa , (bb | cc))"
    test = parse(chk) == __to_set([["aa" , "bb"] , ["aa" , "cc"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    chk = "(aa , (bb | cc))"
    test = parse(chk) == __to_set([["aa" , "bb"] , ["aa" , "cc"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(a , (b | c))"
    test = parse(chk) == __to_set([["a" , "b"] , ["a" , "c"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    
    chk = "(a , (b | (c | d)))"
    test = parse(chk) == __to_set([["a" , "b"] , ["a" , "c"] , ["a" , "d"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(a | ((((b))) | (c | d)))"
    test = parse(chk) == __to_set([["a"] , ["b"] , ["c"] , ["d"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(a | (b | ((c) | d)))"
    test = parse(chk) == __to_set([["a"] , ["b"] , ["c"] , ["d"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "((a , b)"
    try:
        parse(chk)
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
    except:
        print "[OK]\tChecking syntax in :" , chk

    chk = "(a , b)"
    test = parse(chk) == __to_set([["a" , "b"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "((a , b , cc) , (a , b , cc))"
    test = parse(chk) == __to_set([["a" , "b" , "cc" , "a" , "b" , "cc"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(aaa)"
    test = parse(chk) == __to_set([["aaa"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "aaa"
    test = parse(chk) == __to_set([["aaa"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(A , (B , C))"
    test = parse(chk) == __to_set([["A" , "B" , "C"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(A , ((B , D) , C))"
    test = parse(chk) == __to_set([["A" , "B" , "D" , "C"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(A , ((B , (D | E)) , C))"
    test = parse(chk) == __to_set([["A" , "B" , "D" , "C"] , ["A" , "B" , "E" , "C"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "(A , (((B | E) , D) , C))"
    test = parse(chk) == __to_set([["A" , "B" , "D" , "C"] , ["A" , "E" , "D" , "C"]])
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "((A | B | C) , (A | B | C))"
    test = parse(chk) == __to_set(
            [
                ['A' , 'A'] , ['A' , 'B'] , ['A' , 'C'] , 
                ['B' , 'A'] , ['B' , 'B'] , ['B' , 'C'] ,
                ['C' , 'A'] , ['C' , 'B'] , ['C' , 'C']
            ]
            )
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "((A | A | C) , (A | B | C))"
    #print "chk : " , chk
    #print "parse(chk) : " , parse(chk)
    test = parse(chk) == __to_set(
            [
                ['A' , 'A'] , ['A' , 'B'] , ['A' , 'C'] , 
                ['C' , 'A'] , ['C' , 'B'] , ['C' , 'C']
            ]
            )
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)

    chk = "((A | B | C) , (B | B | B))"
    #print "chk : " , chk
    #print "parse(chk) : " , parse(chk)
    test = parse(chk) == __to_set(
            [
                ['A' , 'B'] , ['B' , 'B'] , ['C' , 'B'] 
            ]
            )
    if(test):
        print "[OK]\tParsing: " , chk
    else:
	iframeinfo = getframeinfo(currentframe())
	print "[FAIL] " + str(iframeinfo)
if(__name__ == "__main__"):
    test_algebraictypeparser()
    #chk = [',', 'aa', 'bb', 'cc', [',', 'ff', ['|', 'gg', 'jj'], 'hh']]
    #res = __expand(chk)
    #print res
    #res = __expand(res)
    #print res
    #res = __expand(res)
    #print res
    #res = __expand(res)
    #print res

    # TODO - add this test
    #chk = "((A | B | C) , (A | B | C))"
    #print parse(chk)
    
    #chk = "(A , (((B | E) , D) , C))"
    #print parse(chk)

    #chk = "(A , ((B , (D | E)) , C))"
    #print parse(chk)
# TODO
# - parse arguments
#   - test - tests the module
