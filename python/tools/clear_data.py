# -*- coding: utf-8 -*-

import os
import argparse
import sys
import ConfigParser
import shutil
import datetime
import subprocess

from partype import clevdata

#####################################################
# NOTATION:                                         #
#                                                   #
# <manifest directory>:                             #
#    Directory containing the ...manifest file.     #
#####################################################

# Deletes data in:
#    <manifest directory>/output
#    <manifest directory>/work
#    <manifest directory>/jobs
# directories.

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            "Deletes data in the -jobs-, -output- and -work- directories."
            " If any of these directories don\'t exist in the directory"
            " containing the manifest an exception is raised.")

    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    args = parser.parse_args()

    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #                                       #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################
    
    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    cdat = clevdata.clevData(manifest_directory)
    cdat.check_all()
    
    #########################################
    # Log file in manifest directory.       #
    #########################################

    logfile = os.path.join(cdat.get_manifest_directory() , "log")

    #########################################
    # Performing the cleanup.               #
    #########################################
    
    print "[clear_data] CLEARING JOBS:"
    for f in os.listdir(os.path.join(cdat.get_manifest_directory() , "jobs")):
        if(os.path.isfile(os.path.join(cdat.get_manifest_directory() , "jobs" , f))):
            print "[clear_data]    DELETING FILE: " , os.path.join(cdat.get_manifest_directory() , "jobs" , f)
            os.remove(os.path.join(cdat.get_manifest_directory() , "jobs" , f))
        if(os.path.isdir(os.path.join(cdat.get_manifest_directory() , "jobs" , f))):
            print "[clear_data]    DELETING DIRECTORY: " , os.path.join(cdat.get_manifest_directory() , "jobs" , f)
            shutil.rmtree(os.path.join(cdat.get_manifest_directory() , "jobs" , f))
  
    print "[clear_data] CLEARING OUTPUT:"
    for f in os.listdir(os.path.join(cdat.get_manifest_directory() , "output")):
        if(os.path.isfile(os.path.join(cdat.get_manifest_directory() , "output" , f))):
            print "[clear_data]    DELETING FILE: " , os.path.join(cdat.get_manifest_directory() , "output" , f)
            os.remove(os.path.join(cdat.get_manifest_directory() , "output" , f))
        if(os.path.isdir(os.path.join(cdat.get_manifest_directory() , "output" , f))):
            print "[clear_data]    DELETING DIRECTORY: " , os.path.join(cdat.get_manifest_directory() , "output" , f)
            shutil.rmtree(os.path.join(cdat.get_manifest_directory() , "output" , f))

    print "[clear_data] CLEARING WORK:"
    for f in os.listdir(os.path.join(cdat.get_manifest_directory() , "work")):
        if(os.path.isfile(os.path.join(cdat.get_manifest_directory() , "work" , f))):
            print "[clear_data]    DELETING FILE: " , os.path.join(cdat.get_manifest_directory() , "work" , f)
            os.remove(os.path.join(cdat.get_manifest_directory() , "work" , f))
        if(os.path.isdir(os.path.join(cdat.get_manifest_directory() , "work" , f))):
            print "[clear_data]    DELETING DIRECTORY: " , os.path.join(cdat.get_manifest_directory() , "work" , f)
            shutil.rmtree(os.path.join(cdat.get_manifest_directory() , "work" , f))

    print "[clear_data] CLEARING MANIFEST LOG FILE: " , logfile
    with open(logfile , "w") as lf:
        lf.write("Log file cleared: " + str(datetime.datetime.now()) + "\n")
    
    print "[clear_data] CLEARING GLOBAL LOG FILE: " , os.path.join(os.environ['DIR'] , 'log')
    with open(os.path.join(os.environ['DIR'] , 'log') , "w") as lf:
        lf.write("Log file cleared: " + str(datetime.datetime.now()) + "\n")

    print "[clear_data] DELETING MANIFEST .CREATE_FUNCTION_APPLICATION_DATA FILE : " , os.path.join(cdat.get_manifest_directory() , ".create_function_application_data")
    if(os.path.isfile(os.path.join(cdat.get_manifest_directory() , ".create_function_application_data"))):
        os.remove(os.path.join(cdat.get_manifest_directory() , ".create_function_application_data"))

    print "[clear_data] DELETING FILES FROM WORK DIRECTORIES ON REMOTE MACHINES"
    for mach in os.listdir(os.path.join(cdat.get_manifest_directory() , "machines")):
        with open(os.path.join(cdat.get_manifest_directory() , "machines" , mach) , 'r') as mfile:
            mpath = mfile.readline().strip()
            comm = "'rm -r " + os.path.join(mpath , os.path.basename(cdat.get_manifest_directory()) , "work").strip() + "/* '"
            print "[clear_data] RUNNING: " , " ".join(["ssh" , mach , comm])
            print "[clear_data] CONTINUE? [y/N]" 
            cont = raw_input()
            if(cont.strip() == 'y' or cont.strip() == 'Y'):
                try:
                    output = subprocess.check_output(" ".join(["ssh" , mach , comm]) , shell = True)
                except:
                    print "[clear_data] PROBLEM TRYING TO RUN COMMAND, WORK DIRECTORY COULD BE EMPTY"

