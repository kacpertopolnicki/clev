# -*- coding: utf-8 -*-

import os
import argparse
import sys
import tempfile
import ConfigParser
import subprocess
import uuid
import shutil
import random

#import parsemanifest
from partype import parsemanifest
from partype import clevdata

#####################################################
# NOTATION:                                         #
#                                                   #
# <manifest directory>:                             #
#    Directory containing the ...manifest file.     #
# <index>:                                          #
#    First positional argument. A string that is    #
#    unique to the newly created data.              #
# <type>:                                           #
#    Second positional argument, one of the types   #
#    defined in the manifest.                       #
# <clev directory>:                                 #
#    Main cluster directory.                        #
# <output directory>:                               #
#    <manifest directory>/output                    #
#####################################################

# Checks if the necessary inputs, given as arguments
#    -a
# to the script are available in:
#    <output directory>
# If the inputs are not available then returns:
#    10
# If inputs are available creates a script:
#    <manifest directory>/<index>
# that will run the function. The second line
# of this script contains information for:
#    <clev directory>/python/tools/cluster_deamon.py
# that determines if it will be ran 
# localy (this_machine) or on a remote computer
# (eg. someuser@some_machine).
# After being picked up by the deamon and ran
# localy or on a remote machine, the result of
# the function application will be written to:
#    <manifest directory>/output/
#    └── <index>
#        ├── 0_data
#        ├── 1_data
#        ├── 2_data
#        ├── ...
#        ├── 0_type
#        ├── 1_type
#        ├── 2_type
#        ├── ...
#        └── bottom
# where:
#    0_data
#    1_data
#    2_data
#    ...
# are element of a typle resulting from the function
# application and each contains text, binary, image, ... 
# data and
#    0_type
#    1_type
#    2_type
#    ...
# contain the type of the elements in the tuple.


if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            "Creates a script -index- that performes the function application"
            " in the jobs directory -<manifest directory>/jobs/-."
            " If the -jobs- directory does not exist, raises exception."
            " Checks if all necessary input for the function are available"
            " in the output directory -<manifest directory>/output-,"
            " if not returns 10.")

    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    parser.add_argument("-a" , "--arguments" , nargs = "*" , help = 
            "A list of strings (typically uuid4) that are an index for the new type."
            " The string can not contain whitespaces.")

    parser.add_argument("function" , help = "Function name.")
    
    parser.add_argument("index" , help = "Result index.")

    parser.add_argument("-c" , "--computer" , help = "Run job on COMPUTER.")

    parser.add_argument("-q" , action = 'store_true' , 
        help = "Put the job in queue instead of trying to submit it.")

    args = parser.parse_args()
    
    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################
    
    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    clevdata.clevData.set_quiet()

    cdat = clevdata.clevData(manifest_directory)

    if(not cdat.check_directories()):
        raise RuntimeError("Could not find or create necessary directories.")

    #########################################
    # Log file in manifest directory.       #
    #########################################

    logfile = os.path.join(cdat.get_manifest_directory() , "log")
   
    if(None == args.arguments):
        args.arguments = []

    #########################################
    # Parse the manifest file and get the   #
    # functions. Check if function from:    #
    #    args                               #
    # is in the manifest.
    #                                       #
    # TODO This could be made faster,       #
    # TODO right now the manifest is parsed #
    # TODO for each function call.          #
    #########################################

    if(not cdat.check_functions_types()):
        raise RuntimeError("Error loading functions from manifest.")

    functions = cdat.get_functions() #parsemanifest.getfunctions(args.manifest)

    if(not args.function in functions):
        with open(logfile , "a") as lf:
            lf.write("Function not in manifest.\n")
        raise RuntimeError("Function not in manifest.")

    function = functions[args.function]

    try:
        function_path = cdat.get_function_paths()[args.function] #cdat.get_config().get("FUNCTION_PATHS" , args.function)
    except:
        with open(logfile , "a") as lf:
            lf.write("Function path not in manifest.\n")
        raise RuntimeError("Function path not in manifest.")

    #########################################
    # Check if necessary directories exist. #
    #########################################
    
    if(not cdat.check_directories()):
        with open(logfile , "a") as lf:
            lf.write("Inside -create_function_application-. -work- directory does not exist.\n")
        raise RuntimeError("Inside -create_function_application-. -work- directory does not exist.")

    #########################################
    # If                                    #
    #    -q                                 #
    # is set add job to queue and exit      #
    # with 0.                               #
    #########################################

    if(args.q):
        with open(os.path.join(cdat.get_manifest_directory() , "jobs" , os.environ["CRT_PREFIX"] + args.index) , "w") as qj:
            qj.write("+++".join(filter(lambda x : x.strip() != "-q" , sys.argv)))
        os.rename(os.path.join(cdat.get_manifest_directory() , "jobs" , os.environ["CRT_PREFIX"] + args.index),
               os.path.join(cdat.get_manifest_directory() , "jobs" , os.environ["CLEV_QUEUE"] + args.index))
        sys.exit(0)

    #########################################
    # Check if function arguments exist in: #
    #    <output directory>                 #
    # directory. If not just exit and       #
    # return 10.                            #
    #########################################
    
    for arg in args.arguments:
        try:
            os.stat(os.path.join(cdat.get_manifest_directory() , "output" , arg))
        except:
            #with open(logfile , "a") as lf:
                #lf.write("Missing inputs: ")
                #lf.write(str(args.arguments) + "\n")
            sys.exit(10) 
    
    #########################################
    # Concatenate all bottom files from     #
    # all arguments to:                     #
    #    bottom                             #
    # If a bottom file does not exist for   #
    # some argumnet then raise:             #
    #    RuntimeError                       #
    #########################################

    bottom = ""
    for arg in args.arguments:
        try:
            os.stat(os.path.join(cdat.get_manifest_directory() , "output" , arg , "bottom"))
            with open(os.path.join(cdat.get_manifest_directory() , "output" , arg , "bottom") , "r") as btt:
                bottom = bottom + btt.read()
        except:
            with open(logfile , "a") as lf:
                lf.write("No bottom file found in " + arg + "\n")
            raise RuntimeError("No bottom file found in " + arg)

    #########################################
    # Check if the data matches the         #
    # types.                                #
    #########################################

    if(bottom.strip() == ""):
        signature = []
        for arg in args.arguments:
            dt = filter(lambda x : len(x.split("_")) == 2 , os.listdir(os.path.join(cdat.get_manifest_directory() , "output" , arg)))
            dat = filter(lambda x : x.split("_")[1] == "data" , dt)
            tpe = filter(lambda x : x.split("_")[1] == "type" , dt)
            dat.sort(key = lambda x : int(x.split("_")[0]))
            tpe.sort(key = lambda x : int(x.split("_")[0]))
            for t in tpe:
                read_type = None
                with open(os.path.join(cdat.get_manifest_directory() , "output" , arg , t) , "r") as type_f:
                    read_type = type_f.read()
                signature.append(read_type.strip())
        if(not (tuple(signature) in function[0])):
            bottom = bottom + "\n[create_function_application]\nArgument types do not match function signatiure in " + args.function + "\n"
            bottom = bottom + "In job: " + str(args.index) + "\n"
            bottom = bottom + "Expectiong: " + str(function[0]) + "\n"
            bottom = bottom + "Got: " + str(tuple(signature)) + "\n"

    #########################################
    # Write expected types to:              #
    #    <output directory>                 #
    # Files with expected types             #
    # will start with:                      #
    #    <ET_TEMP>                          #
    # prefix defined in                     #
    #    <clev directory>/cluster_env       #
    #########################################
    
    with open(os.path.join(cdat.get_manifest_directory() , "output" , os.environ["ET_TEMP"] + args.index) , "w") as et:
        for t in function[1]:
            for al in t:
                et.write(al + " ")
            et.write("\n")

    #########################################
    # Read environment variables from       #
    #    <clev directory>/cluster_env       #
    #########################################
    
    envvar = None
    with open(os.path.join(os.environ['DIR'] , 'cluster_env') , 'r') as env:
        envvar = env.read()

    #########################################
    # Chose a machine to run the function.  #
    #                                       #
    # TODO - this can be improved           #
    #########################################
   
#    job_number = None
#    try:
#        with open(os.path.join(cdat.get_manifest_directory() , ".create_function_application_data") , "r") as dta:
#            job_number = int(dta.read().strip())
#    except IOError:
#        job_number = -1
#    job_number = job_number + 1
#    with open(os.path.join(cdat.get_manifest_directory() , ".create_function_application_data") , "w") as dta:
#        dta.write(str(job_number))
#
#    available_machines = os.listdir(os.path.join(cdat.get_manifest_directory() , "machines"))
#    available_machines.append("this_machine") 
#    mach_number = job_number % len(available_machines)
#    mach = available_machines[mach_number]
#    #mach = random.choice(available_machines)

    mach = "this_machine"
    if(args.computer != None):
        mach = args.computer.strip()
    print "[create_function_application] CREATING FUNCTION APPLICATION ON: " , mach
    dirname = os.path.basename(cdat.get_manifest_directory())

    new_manifest_directory = cdat.get_manifest_directory()
    if(mach.strip() != "this_machine"):
        with open(os.path.join(cdat.get_manifest_directory() , "machines" , mach) , "r") as m:
            new_manifest_directory = os.path.join(m.readline().strip() , dirname)
    
    #########################################
    # Creating the tar file with the        #
    # function arguments.                   #
    # If there is a non empty bottom then   #
    # only the bottom will be in the tar    #
    # payload of the script.                #
    #########################################
    
    tarpath = tempfile.mktemp()
    tar_options = "-c"
     
    function_name = ""
    argument_names = ""

    if(bottom.strip() == ""):
        ii_data = 0
        ii_type = 0
        for arg in args.arguments:
            #print "----"
            #print arg
            dt = filter(lambda x : len(x.split("_")) == 2 , 
                    os.listdir(os.path.join(cdat.get_manifest_directory() , "output" , arg)))
            dat = filter(lambda x : x.split("_")[1] == "data" , dt)
            tpe = filter(lambda x : x.split("_")[1] == "type" , dt)
            dat.sort(key = lambda x : int(x.split("_")[0]))
            tpe.sort(key = lambda x : int(x.split("_")[0]))
            if(len(dat) != len(tpe)):
                with open(logfile , "a") as lf:
                    lf.write("Number of data files does not match number of typefiles.\n")
                raise ValueError("Number of data files does not match number of type files.")
            for d in dat:
                argument_names = argument_names + os.path.join(new_manifest_directory , "work" , os.environ["CFA_TEMP"] + args.index , str(ii_data) + "_data") + " " 
                subprocess.call(
                        [
                            "tar" ,
                            "-f" , tarpath , 
                            "-C" , os.path.join(cdat.get_manifest_directory() , "output" , arg) ,
                            tar_options , d , 
                            "--transform" , "s/" + d + "/" + str(ii_data) + "_data/"
                        ])
                if("-c" == tar_options):
                    tar_options = "-r"
                ii_data = ii_data + 1
            for t in tpe:
                with open(os.path.join(cdat.get_manifest_directory() , "output" , arg , t) , "r") as t_val:
                    function_name = function_name + t_val.read().strip() + ","
                t_from = os.path.join(cdat.get_manifest_directory() , "output" , arg , t)
                subprocess.call(
                        [
                            "tar" ,
                            "-f" , tarpath , 
                            "-C" , os.path.join(cdat.get_manifest_directory() , "output" , arg) ,
                            tar_options , t , 
                            "--transform" , "s/" + t + "/" + str(ii_type) + "_type/"
                        ])
                if("-c" == tar_options):
                    tar_options = "-r"
                ii_type = ii_type + 1
            with open(os.path.join(cdat.get_manifest_directory() , "output" , arg , "bottom") , "r") as btt:
                bottom = bottom + btt.read()
    
        temp = tempfile.mktemp()
        with open(temp , "w") as btt:
            btt.write(bottom.strip())
        subprocess.call(
                [
                    "tar" ,
                    "-f" , tarpath , 
                    "-C" , os.path.dirname(temp) ,
                    tar_options , os.path.basename(temp) , 
                    "--transform" , "s/" + os.path.basename(temp) + "/" + "bottom/"
                ])
        os.remove(temp)
    
    #########################################
    # Writing the script.                   #
    #########################################
  
    function_name = os.path.join(new_manifest_directory , cdat.get_manifest_name() ,
            function_path , function_name[0:-1])

    temp_script_path = os.path.join(cdat.get_manifest_directory() , "jobs" , os.environ["CRT_PREFIX"] + args.index)

    script = None

    if(bottom.strip() == ""):
        handleKill = """
# Sauce: https://unix.stackexchange.com/questions/124127/kill-all-descendant-processes
list_descendants ()
{
  local children=$(ps -o pid= --ppid "$1")

  for pid in $children
  do
    list_descendants "$pid"
  done
  
  if [ ! "$children" = "" ]
  then
    echo "$children"
  fi
}

handle_kill ()
{
  kill $(list_descendants $$)
}

trap 'handle_kill' TERM
        """
        script = "#!" + os.environ["CLUSTER_BASH"] + "\n# " + mach + "\n\n" + envvar + "\n"
        script = script + "echo " + args.index + " $$ >> " + os.path.join(new_manifest_directory , "work" , "pids") + "\n"
        script = script + handleKill
        script = script + """# Sauce:
# https://www.linuxjournal.com/content/add-binary-payload-your-shell-scripts

match=$(grep --text --line-number '^""" + os.environ["CLUSTER_PAYLOAD"] + """:$' $0 | cut -d ':' -f 1)
payload_start=$((match + 1))
mkdir """ + os.path.join(new_manifest_directory , "work" , os.environ["CFA_TEMP"] + args.index) + """
if [[ $? -eq 0 ]]
then
    tail -n +$payload_start $0 | tar -x -f - -C """ + os.path.join(new_manifest_directory , "work" , os.environ["CFA_TEMP"] + args.index) + """
    if [[ $? -eq 0 ]]
    then
        """ + function_name + " " + argument_names + """
        if [[ $? -eq 0 ]]
        then
            rm -r """ + os.path.join(new_manifest_directory , "work" , os.environ["CFA_TEMP"] + args.index) + """
        else
            exit 1
        fi
    else
        exit 1
    fi
else
    exit 1
fi
""" 
        script = script + "exit 0\n\n"
        script = script + os.environ["CLUSTER_PAYLOAD"] + ":\n"

        #print "Creating script."
        
        with open(temp_script_path , 'w') as temp_script:
            temp_script.write(script)
            with open(tarpath , "rb") as tf:
                temp_script.write(tf.read())

        os.remove(tarpath)
    else:
        script = "#!" + os.environ["CLUSTER_BASH"] + "\n# " + "this_machine" + "\n" + envvar + "\n"
        script = script + """# Sauce:
# https://www.linuxjournal.com/content/add-binary-payload-your-shell-scripts

match=$(grep --text --line-number '^""" + os.environ["CLUSTER_PAYLOAD"] + """:$' $0 | cut -d ':' -f 1)
payload_start=$((match + 1))
tail -n +$payload_start $0 
""" 
        script = script + "exit 0\n\n"
        script = script + os.environ["CLUSTER_PAYLOAD"] + ":\n"
        script = script + os.environ["CLUSTER_BEGIN_BOTTOM"]
        script = script + "\n[create_function_application]\nIn job: " + args.index + "\n\n" + bottom.strip() + "\n\n"
        script = script + os.environ["CLUSTER_END_BOTTOM"]

        #print "Creating script."
        
        with open(temp_script_path , 'w') as temp_script:
            temp_script.write(script)
    
    #########################################
    # Renaming:                             #
    #    <CRT_PREFIX><index>                #
    # script to:                            #
    #    <index>                            #
    # This renaming ensures that the        #
    # cluster deamon does not run           #
    # incomplete scripts.                   #
    #########################################
    
    script_path = os.path.join(cdat.get_manifest_directory() , "jobs" , args.index)

    os.rename(temp_script_path , script_path)
    
    sys.exit(0)
