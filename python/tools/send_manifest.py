# -*- coding: utf-8 -*-

import os
import argparse
import sys
import tempfile
import ConfigParser
import subprocess
import uuid
import shutil
import stat
import datetime

#import parsemanifest
from partype import clevdata
from partype import parsemanifest

#####################################################
# NOTATION:                                         #
#                                                   #
# <manifest directory>:                             #
#    Directory containing the ...manifest file.     #
#####################################################

# Runs 
#    make clean
# if 
#    Makefile 
# exists inside:
#    <manifest_directory>
# Makes a tar archive, sends it to a remote machine.
# Extracts the archive on the remote machine and runs
#    make
# If these steps are succesfull then adds a 
# new machine to the
#    <manifest direcotry>/machines
# directory. For more information run:
#    runclevtool send_manifest -h

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            "Checks if all types and functions from manifest are available."
            " Additionally checks that the: work, out, job and machine"
            " dirrectories exist. If all is ok attempts to compile manifest"
            " functions and types on remote machine.")

    parser.add_argument("adress" , 
            help = "SSH adress of remote machine."
            " Paswordless access is assumed for this machine.")

    parser.add_argument("fullpath" , 
            help = "Full path to existing folder on remote machine.")
 
    parser.add_argument("version" , 
            help = "String with the version of the code.")   
    
    parser.add_argument("weight" , 
            help = "The number of jobs submitted to remote machine"
            " will be proportional to weight. This local machine has"
            " weight 1.0.")   
    
    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    args = parser.parse_args()

    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################
    
    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    cdat = clevdata.clevData(manifest_directory)
    cdat.check_all()

    ok1 = True   # make clean works
    ok2 = True   # archive successfully created 
    ok3 = True   # archive successfully sent
    ok4 = True   # archive successfully extracted and built

    if(cdat.ok):
        print "[send_manifest] NO ERRORS FOUND"
        
        print "[send_manifest] RUNNING MAKE CLEAN"

        ok1 = cdat.run_make_clean()

        if(ok1):
            tarpath = tempfile.mktemp()
            print "[send_manifest] CREATING ARCHIVE : " , tarpath + ".tar.gz"
            try:
                updir = os.path.abspath(os.path.join(cdat.get_manifest_directory() , ".."))
                dirname =os.path.basename(cdat.get_manifest_directory())
                ok2 = (subprocess.call(["tar" , "-C" , updir , "-czf" , tarpath + ".tar.gz" , dirname]) == 0)
                if(ok2):
                    print "[send_manifest] -OK-    Archive created."
                    print "[send_manifest] SENDING ARCHIVE TO : " , args.adress + ":" + args.fullpath
                    ok3 = (subprocess.call(["scp" , tarpath + ".tar.gz" , args.adress + ":" + args.fullpath]) == 0)
                    if(not ok3):
                        print "[send_manifest] -ERROR- Could not send archive to remote machine."
                    else:
                        print "[send_manifest] -OK-    Archive sent to remote machine."
                        print "[send_manifest] EXTRACTING ARCHIVE ON REMOTE MACHINE"
                        #if(raw_input("EXECUTING: \n    cd " + args.fullpath + "; rm -rf " + dirname + " \nON REMOTE MACHINE. \nSKIP THIS STEP [Y/n]?").lower() == "n"):
                        #    print "[send_manifest] REMOVING PREVIOUS VERSION FROM REMOTE MACHINE"
                        print "[send_manifest] EXECUTING: \n    cd " + args.fullpath + "; rm -rf " + dirname + " \n     ON REMOTE MACHINE."
                        ok4 = ok4 and (subprocess.call(["ssh" , args.adress , "cd " + args.fullpath + "; rm -rf " + dirname]) == 0);
                        ok4 = ok4 and (subprocess.call(["ssh" , args.adress , "cd " + args.fullpath + "; tar -zxvf " + os.path.basename(tarpath + ".tar.gz")]) == 0);
                        ok4 = ok4 and (subprocess.call(["ssh" , args.adress , "cd " + args.fullpath + "; rm " + os.path.basename(tarpath + ".tar.gz")]) == 0);
                        if(cdat.get_has_make()):
                            print "[send_manifest] RUNNING MAKE ON REMOTE MACHINE"
                            make_result = subprocess.call(["ssh" , args.adress , "cd " + os.path.join(args.fullpath , os.path.basename(cdat.get_manifest_directory())) + "; make"])
                            ok4 = ok4 and (make_result == 0);
                            print ok4
                        if(ok4):
                            print "[send_manifest] -OK-    Archive extracted/built/replaced on remote host."
                        else:
                            print "[send_manifest] -ERROR- Could not extract archive on remote host."
                else:
                    print "[send_manifest] -ERROR- Could not create archive."
            finally:
                print "[send_manifest] REMOVING ARCHIVE"
                os.remove(tarpath + ".tar.gz")

    ok = cdat.ok and ok1 and ok2 and ok3 and ok4
    if(ok):
        version = None
        print "[send_manifest] NO ERRORS FOUND"
        with open(os.path.join(cdat.get_manifest_directory() , "machines" , args.adress) , "w") as m:
            m.write(args.fullpath + "\n")
            m.write(str(datetime.datetime.now()) + "\n")
            m.write(args.version.strip() + "\n")
            m.write(args.weight.strip() + "\n")
        sys.exit(0)
    else:
        print "[send_manifest] SOME ERRORS FOUND"
        #TODO
        print os.path.join(cdat.get_manifest_directory() , "machines" , args.adress + ":" + args.fullpath)
        sys.exit(1)
    
    sys.exit(0)
