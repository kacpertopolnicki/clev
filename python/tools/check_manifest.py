# -*- coding: utf-8 -*-

import os
import argparse
import sys
import tempfile
import ConfigParser
import subprocess
import uuid
import shutil
import stat

#import parsemanifest
from partype import parsemanifest
from partype import clevdata

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            "Checks if all types and functions from manifest are available."
            " Tries to run -make clean- and -make-."
            " Additionally checks that the: -work-, -output-, -job- and -machine-"
            " dirrectories exist. Returns 0 if all is ok, 1 otherwise. ")

    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    args = parser.parse_args()

    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #                                       #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################
    
    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    cdat = clevdata.clevData(manifest_directory)
    cdat.check_all()
    
    if(cdat.ok):
        print "[check_manifest] NO ERRORS FOUND"

        #########################################
        # Write info file.                      #
        # This file schould be easy to parse    #
        # and contains information on the       #
        # functions and types.                  #
        #########################################

        print "[check_manifest] CREATING INFO FILE"
        
        with open(os.path.join(cdat.get_manifest_directory() , "info") , "w") as info:
            for f in cdat.get_functions():
                info.write("#FN\n")
                info.write(f + "\n")
                argument_alternatives = cdat.get_functions()[f][0]
                return_type = cdat.get_functions()[f][1]
                info.write("#RA\n")
                for tple in return_type:
                    for t in tple:
                        info.write(t + " ")
                    info.write("\n")
                info.write("#AA\n")
                for tple in argument_alternatives:
                    for t in tple:
                        info.write(t + " ")
                    info.write("\n")
                info.write("#NF\n")
            for tpe in cdat.get_type_paths():
                info.write("#TP\n")
                info.write(tpe + "\n")

        sys.exit(0)
    else:
        print "[check_manifest] SOME ERRORS FOUND"
        sys.exit(1)
    


