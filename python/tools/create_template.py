# -*- coding: utf-8 -*-

import os
import argparse
import sys
import tempfile
import ConfigParser
import subprocess
import uuid
import shutil
import stat

#import parsemanifest
from partype import parsemanifest
from partype import clevdata

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = "Reads the manifest and creates a template of the"
            " project. The newly created files will need to be replaced with"
            " appropriate executables."
            )
    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    args = parser.parse_args()

    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #                                       #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################
    
    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    cdat = clevdata.clevData(manifest_directory)
    
    if(cdat.ok):
        cdat.make_template()

