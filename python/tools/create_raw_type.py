# -*- coding: utf-8 -*-

import os
import argparse
import sys
import tempfile
import ConfigParser
import subprocess

from partype import clevdata

#####################################################
# NOTATION:                                         #
#                                                   #
# <manifest directory>:                             #
#    Directory containing the ...manifest file.     #
# <index>:                                          #
#    First positional argument. A string that is    #
#    unique to the newly created data.              #
# <type>:                                           #
#    Second positional argument, one of the types   #
#    defined in the manifest.                       #
# <clev directory>:                                 #
#    Main cluster directory.                        #
# <output directory>:                               #
#    <manifest directory>/output                    #
#####################################################

# Reads data (any format: text, binary, image, ....) from:
#    stdin
# for a raw type (one of the types defined in the manifest). 
# Creates script in:
#    <manifest directory>/jobs/<index>
# The second line of this script contains the string:
#    this_machine
# informing the cluster deamon that the script 
# will be ran localy (the cluster deamon is ran
# using the 
#    runclevtool cluster_deamon 
# command.). After being picked up by the deamon
# and ran, the type is checked and the following 
# directory structure will be created:
# that when ran by the 
#    <manifest directory>/output/
#    └── <index>
#        ├── 0_data
#        ├── 0_type
#        └── bottom
# where
#    0_data
# contains the data, it can be a text, image, binary, ... file, 
#    0_type
# is a file containing the type (one of the types defined 
# in the manifest, typically this file contains a single string:
#    <type>
# that was passed as the second positional argument).
#    bottom
# is empty if the type checking produced no errors
# or contains error messages.

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            "Reads data for raw type from stdin. "
            " Creates a script -index- that builds the raw type"
            " in the jobs directory -<manifest directory>/jobs/-."
            " If the -jobs- directory does not exist, raises exception.")

    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    parser.add_argument("index" , help = 
            "A string (typically uuid4) that is an index for the new type."
            " The string can not contain whitespaces.")

    parser.add_argument("type" , help = "Name of raw type.")

    args = parser.parse_args()
    
    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################

    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest
    
    clevdata.clevData.set_quiet()
    
    cdat = clevdata.clevData(manifest_directory)
    
    if(not cdat.check_directories()):
        raise RuntimeError("Could not find or create necessary directories.")

    #########################################
    # Log file in manifest directory.       #
    #########################################

    logfile = os.path.join(cdat.get_manifest_directory() , "log")
    
    #########################################
    # Checking if type in manifest.         #
    # If not, raises RuntimeError.          #
    #########################################
    
    if(not args.type in cdat.get_config().options("RAW_TYPES")):
        with open(logfile , "a") as lf:
            lf.write("Argument to -create_raw_type- not in -RAW_TYPES- section of manifest.\n")
        raise ValueError("Argument to -create_raw_type- not in -RAW_TYPES- section of manifest.")

    #########################################
    # Checking if the first positional      #
    # argument has whitespaces.             #
    #########################################
    
    args.index = args.index.strip()

    if((' ' in args.index) or ('\t' in args.index) or ('\n' in args.index)):
        with open(logfile , "a") as lf:
            lf.write("Raw type index in -create_raw_type- contains whitespaces.\n")
        raise ValueError("Raw type index in -create_raw_type- contains whitespaces.")

    #########################################
    # Setting path to the programm that     #
    # will check the type.                  #
    #########################################
    
    type_check_programm = os.path.join(
            cdat.get_manifest_directory() , 
            cdat.get_manifest_name() , 
            cdat.get_config().get("RAW_TYPES" , args.type) , 
            args.type)

    #########################################
    # Reading data from stdin into a        #
    #    data                               #
    #########################################

    data = sys.stdin.read()
 
    #########################################
    # Read environment variables from       #
    #    <clev directory>/cluster_env       #
    #########################################
    
    envvar = None
    with open(os.path.join(os.environ['DIR'] , 'cluster_env') , 'r') as env:
        envvar = env.read()

    script = "#!" + os.environ["CLUSTER_BASH"] + "\n# this_machine\n\n" + envvar + "\n"

    script = script + """# Sauce:
# https://www.linuxjournal.com/content/add-binary-payload-your-shell-scripts

match=$(grep --text --line-number '^""" + os.environ["CLUSTER_PAYLOAD"] + """:$' $0 | cut -d ':' -f 1)
payload_start=$((match + 1))
tail -n +$payload_start $0 
""" #> """
    script = script + "exit 0\n\n"
    script = script + os.environ["CLUSTER_PAYLOAD"] + ":\n"
    
    #########################################
    # Create a script that writes the type  #
    # to stdout or writes a bottom to       #
    # stdout.                               #
    #########################################
    
    script = script + os.environ["CLUSTER_BEGIN_TYPE"] + args.type.strip() + os.environ["CLUSTER_END_TYPE"] + "\n"
    script = script + os.environ["CLUSTER_BEGIN_DATA"] + data + os.environ["CLUSTER_END_DATA"] + "\n"
    
    #########################################
    # Raise:                                #
    #    RuntimeError                       #
    # if:                                   #
    #    <manifest directory>/jobs          #
    # or:                                   #
    #    <manifest directory>/output        #
    # directories do not exist.             #
    #########################################
    
    if(not os.path.isdir(os.path.join(cdat.get_manifest_directory() , "jobs"))):
        with open(logfile , "a") as lf:
            lf.write("In -create_raw_type-, the -jobs- directory does not exist in -" + cdat.get_manifest_directory() + "-\n")
        raise RuntimeError("In -create_raw_type-, the -jobs- directory does not exist in -" + cdat.get_manifest_directory() + "-\n")

    if(not os.path.isdir(os.path.join(cdat.get_manifest_directory() , "output"))):
        with open(logfile , "a") as lf:
            lf.write("In -create_raw_type-, the -output- directory does not exist in -" + cdat.get_manifest_directory() + "-\n")
        raise RuntimeError("In -create_raw_type-, the -output- directory does not exist in -" + cdat.get_manifest_directory() + "-\n")
    
    #########################################
    # Writing expected type to              #
    #    <output directory>/<ET_TEMP>index  #
    # where                                 #
    #    <ET_TEMP>                          #
    # is a prefix defined in                #
    #    <clev directory>/cluster_env       #
    #########################################

    with open(os.path.join(cdat.get_manifest_directory() , "output" , os.environ["ET_TEMP"] + args.index) , "w") as et:
        et.write(args.type.strip() + " ")
        et.write("\n")
    
    #########################################
    # Writing script to temporary file:     #
    #    <CRT_PREFIX><index>                #
    # where                                 #
    #    <CRT_PREFIX>                       #
    # is an environment variable set in     #
    #    <clev directory>/cluster_env       # 
    #########################################
    
    temp_script_path = os.path.join(cdat.get_manifest_directory() , "jobs" , os.environ["CRT_PREFIX"] + args.index)
    with open(temp_script_path , 'w') as temp_script:
        temp_script.write(script)

    #########################################
    # Renaming:                             #
    #    <CRT_PREFIX><index>                #
    # script to:                            #
    #    <index>                            #
    # This renaming ensures that the        #
    # cluster deamon                        #
    #    <clev directory>/python/tools/     #
    #        cluster_deamon.py              #
    # does not run                          #
    # incomplete scripts.                   #
    #########################################
    
    script_path = os.path.join(cdat.get_manifest_directory() , "jobs" , args.index)

    os.rename(temp_script_path , script_path)

    sys.exit(0)
