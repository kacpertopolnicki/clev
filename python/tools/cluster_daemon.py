# -*- coding: utf-8 -*-

import os
import argparse
import subprocess
import sys
import time
import ConfigParser
import time
import random

from partype import clevdata

#####################################################
# NOTATION:                                         #
#                                                   #
# <manifest directory>:                             #
#    Directory containing the ...manifest file.     #
# <index>:                                          #
#    First positional argument. A string that is    #
#    unique to the newly created data.              #
# <clev directory>:                                 #
#    Main cluster directory.                        #
# <output directory>:                               #
#    <manifest directory>/output                    #
#####################################################

# Runs cluster deamon loop. In each iteration the:
#    <manifest directory>/jobs
# directory is scanned for jobs to be executed.
# The outputs parsed and written to:
#    <manifest directory>/output


if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            "Run cluster deamon. The deamon will rub jobs from"
            " the -input- directory and write the output to the"
            " -output- directory.")

    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " Manifest files have the extension -.manifest- and"
            " contain information on the RAW types,"
            " the ALGEBRAIC types and FUNCTION signatures." 
            " Dummy code will be written to the manifests parent"
            " directory. A new directory will be created for the code"
            " that has the same name as the manifest. For example if "
            " the manifest is -some_functions.manifest- then the new"
            " directory will be named -some_functions-. If this argument"
            " is not specified, the current directory will"
            " be searched for the manifest.",
            type = str)

    parser.add_argument("-d" , "--debug" , 
            help = 
            "If this argument is given then, instead of removing, "
            "all job files will be copied to this directory.")

    parser.add_argument("-n" , "--njobs" , 
            help = 
            "If this argument is given then, a maximum number"
            " of running jobs will be set to the given value."
            " If not the default value of 4 will be used.")
    
    parser.add_argument("-l" , "--ljobs" , 
            help = 
            "If this argument is given then, a maximum number"
            " of unqueud jobs running on the local machine will"
            " be set to the given value."
            " If not the default value of 4 will be used.")
    
    args = parser.parse_args()

    if(args.njobs == None):
        args.njobs = 4
    
    if(args.ljobs == None):
        args.ljobs = 4
    
    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################
    
    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    cdat = clevdata.clevData(manifest_directory)
    cdat.check_all()
    
    os.chdir(cdat.get_manifest_directory())

    #########################################
    # Get a list of machines and their      #
    # weights.                              #
    #########################################
    
    available_machines = {"this_machine" : 1.0}
    jobs_on_machines = {"this_machine" : 0}
    total_weight = 1.0

    for mach in os.listdir(os.path.join(cdat.get_manifest_directory() , "machines")):
        with open(os.path.join(cdat.get_manifest_directory() , "machines" , mach.strip()) , "r") as m:
            wgt = float(m.readlines()[-1])
            available_machines.update({mach.strip() : wgt})
            jobs_on_machines.update({mach.strip() : 0})
            total_weight = total_weight + wgt

    for mach in available_machines:
        available_machines[mach] = available_machines[mach] / total_weight
    
    #submit_list = []
    #submit_index = 0
    #for m in available_machines:
    #    for ii in range(int(available_machines[m] * float(args.njobs) + 0.5)):
    #        submit_list.append(m)

    submitted = []
    running = {}
    con = True
    numbertorun = None

    numbertoqueue = None
    newqueued = None

    try:
        while(con):
            #print len(running)
        
            #########################################
            # Check for jobs to be ran.             #
            #########################################
            
            outputs = os.listdir(os.path.join(cdat.get_manifest_directory() , "output"))

            jobs = os.listdir(os.path.join(cdat.get_manifest_directory() , "jobs"))

            # Filter out incomplete scripts:
            jobs = filter(lambda x : not (os.environ["CRT_PREFIX"] in x) , jobs)
            # Filter out queued jobs:
            queued = filter(lambda x :     (os.environ["CLEV_QUEUE"] in x) , jobs)
            jobs =   filter(lambda x : not (os.environ["CLEV_QUEUE"] in x) , jobs)
            # Filter out jobs that are finished and parsed:
            jobs = filter(lambda x : not (x                                in outputs) , jobs)
            # Filter out jobs that are finished and not parsed:
            jobs = filter(lambda x : not (os.environ["PA_PREFIX_1"] + x    in outputs) , jobs)
            jobs = filter(lambda x : not (os.environ["PA_PREFIX_2"] + x    in outputs) , jobs)
            # Filter out jobs that are being ran:
            jobs = filter(lambda x : not (x in running) , jobs)
            # Filter out jobs that were already submitted:
            jobs = filter(lambda x : not (x in submitted) , jobs)

            #########################################
            # Try to run unqueud jobs in jobs       #
            # directory. Only jobs to be ran on:    #
            #    this_machine                       #
            # will be ran.                          #
            #########################################
            
            if(args.ljobs != None):
                numbertorun = int(args.ljobs) - len(running)
                if(len(jobs) > numbertorun):
                    jobs = jobs[0:numbertorun]

            for job in jobs:
                try:
                    output_file_path = os.path.join(cdat.get_manifest_directory() , "output" , os.environ["PA_PREFIX_1"] + job)
                    output_file = open(output_file_path , "wb" , -1)
                  
                    runon = None
                    with open(os.path.join(cdat.get_manifest_directory() , "jobs" , job) , "r") as jobscript:
                        jobscript.readline() # #!/bin/bash
                        runon = jobscript.readline()
                        if(len(runon) < 3):
                            sys.stderr.write("Wrong syntax in job script.\n")
                            sys.exit(1)
                        else:
                            runon = runon[2:].strip()
                   
                    print "[cluster_daemon] RUNNING NON QUEUD JOB: " , job
                    print "[cluster_daemon] ON: " , runon
                    
                    process = None
                    if(runon == "this_machine"):
                        try:
                            process_command =  [os.environ["CLUSTER_BASH"] , os.path.join(cdat.get_manifest_directory() , "jobs" , job)]
                            process = subprocess.Popen(process_command , stdout = output_file , stderr = subprocess.PIPE)
                        except:
                            sys.stderr.write("Problem starting job: " + job + " on local machine.\n")
                            sys.stderr.write("Exiting cluster_daemon.\n")
                            sys.exit(1)
                    else:
                            sys.stderr.write("Attempting to run unqueud job: " + job + " on non local machine.\n")
                            sys.stderr.write("Exiting cluster_daemon.\n")
                            sys.exit(1)
                    try:
                        running.update({job : (process , output_file , runon)})
                        submitted.append(job)
                    except:
                        sys.stderr.write("Problem updating job: " + job + "\n")
                        sys.stderr.write("Exiting cluster_daemon.\n")
                        sys.exit(1)
                except:
                    sys.stderr.write("Could not start: " + job + "\n")
                    sys.stderr.write("Exiting cluster_daemon.\n")
                    sys.exit(1)

            #########################################
            # Try to submit queud jobs.             #
            #########################################
           
            if(args.njobs != None):
                numbertoqueue = int(args.njobs) - len(running)
            
            newqueued = 0

            for job in queued:
                if(newqueued < numbertoqueue):
                    #print "[cluster_daemon] Trying to submit: " , job
                    comm = None
                    rc = None
                    with open(os.path.join(cdat.get_manifest_directory() , "jobs" , job) , "r") as que:
                        comm = que.read().strip().split("+++")
                        #print comm

                    for j in running:
                        jobs_on_machines.update({running[j][2] : 0})
                    
                    for j in running:
                        jobs_on_machines.update({running[j][2] : jobs_on_machines[running[j][2]] + 1})
                   
                    #print "[cluster_daemon] " , jobs_on_machines
                    smach = random.choice(list(available_machines))
                    for m in jobs_on_machines:
                        if(float(len(running)) * available_machines[m] > float(jobs_on_machines[m])):
                            smach = m
                            #print "[cluster_deamon] CHOSING MACHINE: " , smach
                            break

                    #smach = submit_list[submit_index]
                    
                    comm = ['python'] + comm[0:3] + ['-c' , smach] + comm[3:]
                    #comm = ['python'] + comm
                    rc = subprocess.call(comm)
                    #print rc
                    #raw_input("?")
                    if(not (rc == 0 or rc == 10)):
                        sys.stderr.write("Error when trying to submit queued job:" + job + ".\n")
                        sys.exit(rc)
                    if(rc == 0):
                        print "[cluster_daemon] JOB SUBMITTED, REMOVING QUEUE FILE: " , job
                        os.remove(os.path.join(cdat.get_manifest_directory() , "jobs" , job))
                        jobn = job.replace(os.environ["CLEV_QUEUE"] , "")
                        try:
                            output_file_path = os.path.join(cdat.get_manifest_directory() , "output" , os.environ["PA_PREFIX_1"] + jobn)
                            output_file = open(output_file_path , "wb" , -1)
                          
                            runon = None
                            with open(os.path.join(cdat.get_manifest_directory() , "jobs" , jobn) , "r") as jobscript:
                                jobscript.readline() # #!/bin/bash
                                runon = jobscript.readline()
                                if(len(runon) < 3):
                                    sys.stderr.write("Wrong syntax in job script.\n")
                                    sys.exit(1)
                                else:
                                    runon = runon[2:].strip()
                            
                            process = None
                            if(runon == "this_machine"):
                                try:
                                    process_command =  [os.environ["CLUSTER_BASH"] , os.path.join(cdat.get_manifest_directory() , "jobs" , jobn)]
                                    process = subprocess.Popen(process_command , stdout = output_file , stderr = subprocess.PIPE)
                                except:
                                    sys.stderr.write("Problem starting job: " + jobn + " on local machine.\n")
                                    sys.stderr.write("Exiting cluster_daemon.\n")
                                    sys.exit(1)
                            else:
                                try:
                                    fullpath = None
                                    with open(os.path.join(cdat.get_manifest_directory() , "machines" , runon) , "r") as mf:
                                        fullpath = mf.readline() # This is ""
                                    fullpath = os.path.join(
                                            fullpath.strip() , 
                                            os.path.basename(cdat.get_manifest_directory()),
                                            "work")
                                    process_command = "scp " +  \
                                        os.path.join(cdat.get_manifest_directory() , "jobs" , jobn) + " " + \
                                        runon + ":" + fullpath
                                    process_command = process_command + "; " + \
                                        "ssh " + runon + " \'bash " + os.path.join(fullpath , jobn) + "\'"
                                    process_command = process_command + "; " + \
                                        "ssh " + runon + " \'rm " + os.path.join(fullpath , jobn) + "\'"
                                    #print "[cluster_daemon] RUNNING"
                                    #print ""
                                    #sys.stdout.write(process_command)
                                    #print ""
                                    #print "[cluster_daemon] DONE"
                                    process = subprocess.Popen(process_command , stdout = output_file , stderr = subprocess.PIPE , shell = True)
                                except:
                                    sys.stderr.write("Could not run: " + jobn + " on " + runon + ".\n")
                                    sys.stderr.write("Exiting cluster_daemon.\n")
                                    sys.exit(1)
                            try:
                                running.update({jobn : (process , output_file , runon)})
                                submitted.append(jobn)
                            except:
                                sys.stderr.write("Problem updating job: " + jobn + "\n")
                                sys.stderr.write("Exiting cluster_daemon.\n")
                                sys.exit(1)
                        except:
                            sys.stderr.write("Could not start: " + jobn + "\n")
                            sys.stderr.write("Exiting cluster_daemon.\n")
                            sys.exit(1)
                        #submit_index = submit_index + 1
                        newqueued = newqueued + 1
                        #if(submit_index > len(submit_list) - 1):
                        #    submit_index = 0

            #########################################
            # Check if jobs are finished.           #
            # If finished close function process    #
            # and parse output.                     #
            #########################################
            
            done = []
            for job in running:
                if(not running[job][0].poll() == None):
                    print "[cluster_daemon] CLOSING JOB: " + job
                    try:
                        if(not running[job][0].poll() == 0):
                            sys.stderr.write("Job: " + job + " returned non zero exit status.\n")
                            sys.stderr.write("Exiting cluster_daemon.\n")
                            sys.exit(1)
                        running[job][1].close()
                        done.append(job)
                        if(args.debug == None):
                            os.remove(os.path.join(cdat.get_manifest_directory() , "jobs" , job))
                        else:
                            os.rename(os.path.join(cdat.get_manifest_directory() , "jobs" , job) , os.path.join(args.debug , job))
                        print "[cluster_daemon] PARSING OUTPUT FROM: " , job
                        parse_command = ['runclevtool' , 'parse_output' , job]
                        parse_command_exit_code = subprocess.call(parse_command)
                        print "[cluster_daemon] EXIT CODE OF PARSE COMMAND: " , parse_command_exit_code
                        if(not 0 == parse_command_exit_code):
                            sys.stderr.write("Could not parse output from: " + job + "\n")
                            sys.stderr.write("Exiting cluster_daemon.\n")
                            sys.exit(1)
                    except:
                        sys.stderr.write("Could not communicate with: " + job + "\n")
                        sys.stderr.write("Exiting cluster_daemon.\n")
                        sys.exit(1)
            for job in done:
                running.pop(job , None) 
          
            #########################################
            # Sleep for a bit.                      #
            #########################################

            time.sleep(0.01)

    finally:
        print "\n[cluster_daemon] EXITING CLUSTER DEAMON."
       
        if(len(running) != 0):
                
            #########################################
            # Get PIDs from all machines.           #
            #########################################

            for mach in available_machines:
                if(mach != "this_machine"):
                    with open(os.path.join(cdat.get_manifest_directory() , "machines" , mach) , 'r') as mfile:
                        try:
                            mpath = mfile.readline().strip()
                            comm = "".join(["'cat " , mpath  , "/" , os.path.basename(cdat.get_manifest_directory()) , "/work/pids'"])
                            print "[cluster_daemon] RUNNING: " , " ".join(["ssh" , mach , comm])
                            output = subprocess.check_output(" ".join(["ssh" , mach , comm]) , shell = True)
                            output =  output.splitlines()
                            output =  map(lambda x : x.split() , output)
                            dic = {key.strip() : (value.strip() , mach) for (key , value) in output}
                            for job in running:
                                if(job in dic):
                                    print "[cluster_daemon] CLOSING JOB WITH PID : " , dic[job][0] , " FROM : " , dic[job][1]
                                    comm1 = "".join(["'pkill -15 -P " , dic[job][0] , "'"])
                                    print "[cluster_daemon] RUNNING: " , " ".join(["ssh" , mach , comm1])
                                    proc1 = subprocess.Popen(" ".join(["ssh" , mach , comm1]) , shell = True , stdout = subprocess.PIPE , stderr = subprocess.PIPE)
                                    output1 , error1 = proc1.communicate()
                                    print "[cluster_daemon] ERROR FROM KILL : " , error1.strip()
                        except:
                            print "[cluster_daemon] COULD NOT GET PIDS FROM: " , mach 

            #########################################
            # Close processes.                      #
            #########################################
            
            for j in running:
                print "[cluster_daemon] CLOSING :" , j
                running[j][1].close()

