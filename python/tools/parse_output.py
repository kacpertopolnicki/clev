# -*- coding: utf-8 -*-

import os
import argparse
import sys
import tempfile
import ConfigParser
import subprocess

from partype import clevdata

#####################################################
# NOTATION:                                         #
#                                                   #
# <manifest directory>:                             #
#    Directory containing the ...manifest file.     #
# <index>:                                          #
#    First positional argument. A string that is    #
#    unique to the newly created data.              #
# <clev directory>:                                 #
#    Main cluster directory.                        #
# <output directory>:                               #
#    <manifest directory>/output                    #
#####################################################

if(__name__ == "__main__"):
    parser = argparse.ArgumentParser(
            description = 
            " Parses the output of a function."
            " The output is read from the:"
            "    <PA_PREFIX_1>_<index>   "
            " file from the output directory. "
            " If the output is: "
            "    <CLUSTER_BEGIN_TYPE>some_type_name<CLUSTER_END_TYPE>,  "
            "    <CLUSTER_BEGIN_DATA>some data for this type<CLUSTER_END_DATA>,  "
            "    <CLUSTER_BEGIN_TYPE>some_other_type_name<CLUSTER_END_TYPE>,  "
            "    <CLUSTER_BEGIN_DATA>some data for other type<CLUSTER_END_DATA>,  "
            " (the <...> contain the contents of environment variables  "
            " set in the cluster_env file from the main cluster directory)"
            " then 5 files will be created in the output directory."
            " These files will be placed in a directory named"
            "    <index>  "
            " (first positional argument)."
            " Files: "
            "    0_type,  "
            "    1_type,  "
            " will contain the type names and:"
            "    0_data,  "
            "    1_data,  "
            " will contain the data. Finally file:"
            "    bottom  "
            " will contain the contents of error messages.")

    parser.add_argument("-m" , "--manifest" , 
            help = 
            "Path to directory file containing the manifest."
            " If this is not set, the current directory will be used.",
            type = str)

    parser.add_argument("index" , help = 
            "A string (typically uuid4) that is an index for the new type."
            " The string can not contain whitespaces.")

    args = parser.parse_args()

    #########################################
    # Determining location of manifest file #
    # and the manifest directory:           #
    #    args.manifest                      #
    #    manifest_directory                 #
    #########################################

    if(args.manifest == None):
        args.manifest = os.getcwd()
    else:
        os.chdir(args.manifest)

    manifest_directory = args.manifest

    cdat = clevdata.clevData(manifest_directory)
    
#    manif = []
#    for f in os.listdir(args.manifest):
#        if(f.split('.')[-1] == 'manifest'):
#            manif.append(f)
#    if(len(manif) != 1):
#        raise RuntimeError('Expecting one manifest file in make_template directory.')
#    manifest_name = manif[0].replace('.manifest' , '')
#    args.manifest = os.path.join(args.manifest , manif[0])
#    
#    #########################################
#    # Parsing manifest file to:             #
#    #    config                             #
#    #########################################
#    
#    config = ConfigParser.ConfigParser()
#    config.optionxform = str
#    config.read(args.manifest)

    #########################################
    # Checking if the first positional      #
    # has whitespaces.                      #
    #########################################
    
    args.index = args.index.strip()

    if((' ' in args.index) or ('\t' in args.index) or ('\n' in args.index)):
        raise ValueError("Raw type index in -create_raw_type- contains whitespaces.")

    #########################################
    # Function output is in the file:       #
    #    <output directory>/                #
    #          <PA_PREFIX_1><index>         #
    # The expected type of the output       #
    # is in the file:                       #
    #    <output directory>/                #
    #           <ET_TEMP><index>            #
    # The parsed output will be written     #
    # to the:                               #
    #    <output directory>/                #
    #           <PA_PREFIX_2><index>        #
    # directory. The strings in             #
    #    <...>                              #
    # are set in the:                       #
    #    <clev directory>/cluster_env       #
    # file.                                 #
    #                                       #
    # Setting the path to the function      #
    # output and the temporary output       #
    # directory:                            #
    #    output_file                        #
    #    output_directory                   #
    # If:                                   #
    #    output_file                        #
    # does not exist, exits with 1.         #
    #########################################
    
    output_file = os.path.join(cdat.get_manifest_directory() , "output" , os.environ["PA_PREFIX_1"] + args.index)
    output_file_et = os.path.join(cdat.get_manifest_directory() , "output" , os.environ["ET_TEMP"] + args.index)
    output_directory = os.path.join(cdat.get_manifest_directory() , "output" , os.environ["PA_PREFIX_2"] + args.index)

    try:
        os.stat(output_file)
    except:
        sys.exit(1)

    try:
        os.stat(output_file_et)
    except:
        raise RuntimeError("Inside -parse_output-. " + args.index + " no " + output_file_et + " file.")
    
    try:
        os.stat(output_directory)
        raise RuntimeError("Inside -parse_output-. " + args.index + " directory already exists.")
    except:
        os.mkdir(output_directory)

    expected_types = []
    with open(output_file_et , "r") as et_f:
        for ln in et_f.readlines():
            altern = map(lambda x : x.strip() , ln.split())
            if (not [] == altern):
                expected_types.append(tuple(altern))

    os.remove(output_file_et)
   
    #########################################
    # Running output through (c programm):  #
    #    <clev directory>/c/tools/          #
    #       separate_output                 #
    # If errors encountered, they will be   #
    # written to the new bottom file.       #
    #                                       #
    # Checking if there is a (possibly      #
    # empty) bottom file. If not            #
    # a:                                    #
    #    RuntimeError                       #
    # is raised.                            #
    #########################################
    
    cat_process = subprocess.Popen(['cat' , output_file] , stdout = subprocess.PIPE)
    separate_output_process = subprocess.Popen(['runclevtool' , 'separate_output' , output_directory] , stdin = cat_process.stdout , stdout = subprocess.PIPE)
    s_out , s_err = separate_output_process.communicate()

    results = []
    for f in os.listdir(output_directory):
        results.append(f.strip())

    if(not 'bottom' in results):
        raise RuntimeError("No bottom file found in -" + args.index + "-\n")
    
    has_bottom = False
    bottom_message = ""

    if(s_err != None):
        has_bottom = True
        bottom_message = bottom_message + "[parse_output]\n" + s_err.strip() + "\n"

    with open(os.path.join(output_directory , "bottom") , "r") as bottom_file:
        error_msg = bottom_file.read()
        if(error_msg.strip() != ""):
            has_bottom = True

    #########################################
    # If no bottom check all types.         #         
    #########################################

    if(not has_bottom):
        dta = {}
        tps = {}
        for f in results:
            if(f.strip() != 'bottom'):
                num , tp = f.split("_")
                if("data" == tp):
                    dta.update({int(num) : tp})
                elif("type" == tp):
                    tps.update({int(num) : tp})

        #########################################
        # Check if each:                        #
        #    <index>_data                       #
        # has the corresponding:                #
        #    <index>_type                       #
        #########################################
        
        if(len(dta) != len(tps)):
            raise RuntimeError("Number of data type files does not match the number of data files.\n")

        for argn in dta:
            if(not argn in tps):
                raise RuntimeError("No type for element " + str(argn) + ".\n")
      
        #########################################
        # Check if return types are             #
        # match the expected return types       #
        #########################################
        
        sign = []
        for ind in range(len(tps)):
            with open(os.path.join(output_directory , str(ind) + "_" + tps[ind]) , "r") as ttt:
                sign.append(ttt.read().strip())
        sign = tuple(sign)

        if(not sign in expected_types):
            bottom_message = bottom_message + "\n[parse_output]\n"
            bottom_message = bottom_message + "Wrong type in " + args.index + "\n"
            bottom_message = bottom_message + "Expecting:\n"
            bottom_message = bottom_message + str(expected_types) + "\n"
            bottom_message = bottom_message + "Got:\n"
            bottom_message = bottom_message + str(sign) + "\n"
        else:
            #########################################
            # Check if return types are             #
            # match the expected return types       #
            #########################################
            for argn in dta:
                argument_type = None
                with open(os.path.join(output_directory , str(argn).strip() + "_type") , "r") as t:
                    argument_type = t.read().strip()
                if(not argument_type in cdat.get_config().options("RAW_TYPES")):
                    raise RuntimeError("Argument " + str(argn) + " has type not in manifest.\n")
                else:
                    print "[parse_output] CHECKING TYPE: " , args.index
                    type_check_programm = os.path.join(
                            cdat.get_manifest_directory() , 
                            cdat.get_manifest_name() , 
                            cdat.get_config().get("RAW_TYPES" , argument_type) , 
                            argument_type)
                    check_out = subprocess.check_output([type_check_programm , os.path.join(output_directory , str(argn).strip() + "_data")])
                    check_out = map(lambda x : x.strip() , check_out.split())
                    type_ok = os.environ["CLUSTER_OK"].strip() in check_out
                    if(not type_ok):
                        bottom_message = bottom_message + "\n" +    \
                                "[parse_output]\n" + \
                                "Type checking programm:\n" + \
                                type_check_programm + "\nencountered an error in  " + args.index + \
                                "\nIn argument number " + str(argn) + ".\n"

    #########################################
    # Update bottom file.                   # 
    #########################################
    
    with open(os.path.join(output_directory , "bottom") , "a") as btm:
        btm.write("\n" + bottom_message)

    #########################################
    # Rename directory with path:           #  
    #    <output directory>/                #
    #       <PA_PREFIX_2><index>            #
    # to:                                   #
    #    <output directory>/<index>
    # This renaming ensures that            #
    # incomplete directories are not used.  #
    #########################################
    
    os.rename(output_directory , os.path.join(cdat.get_manifest_directory() , "output" , args.index))
    os.remove(output_file)

    sys.exit(0)
