# -*- coding: utf-8 -*-

#import parsealgebraictype
#import parsemanifest
from partype import parsealgebraictype
from partype import parsemanifest

if(__name__ == "__main__"):
    #########################################
    # Perform the tests.                    #
    # TODO - add tests for each module      #
    #########################################
    parsealgebraictype.test_algebraictypeparser()
    parsemanifest.test_manifestparser()

