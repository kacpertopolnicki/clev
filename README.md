`CLEV`
=============================================================================

`CLEV` is a system for distributing computations over a network of computers.
Each individual computation is implemented as a user defined program. Data is
sent across the network using standard `ssh` and `scp` protocols.

