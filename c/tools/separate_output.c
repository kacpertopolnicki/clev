#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Some things are repeating in main for:
// data, type and bottom.
//
// Make some data global and write subroutines.

int main(int argc , char * argv[])
{
	if(2 == argc)
	{
		if(strcmp(argv[1] , "-h") == 0 || strcmp(argv[1] , "--help") == 0)
		{
			printf(
					"usage: separate_output [-h] output\n"
					"\n"
					"Reads the output of a cluster function from stdin.\n"
					"Separates the output and writes it in to the -output- directory.\n"
					"If the input read from stdin is:\n"
					"   <CLUSTER_BEGIN_TYPE>some_type_name<CLUSTER_END_TYPE>\n"
					"   <CLUSTER_BEGIN_DATA>some data for this type<CLUSTER_END_DATA>\n"
					"   <CLUSTER_BEGIN_TYPE>some_other_type_name<CLUSTER_END_TYPE>\n"
					"   <CLUSTER_BEGIN_DATA>some data for other type<CLUSTER_END_DATA>\n"
					"Where the <...> contain the contents of environment variables\n"
					"set in -runclustertool-\n"
					"then 5 files will be created in the output directory.\n"
					"Files:\n"
					"   0_type\n"
					"   1_type\n"
					"will contain the type names and:\n"
					"   0_data\n"
					"   1_data\n"
					"will contain the data. Finally file:\n"
					"   bottom\n"
					"will contain the contents of error messages.\n"
					"\n"
					"positional arguments:\n"
					"  output        Path to output directory.\n"
					"\n"
					"optional arguments:\n"
					"  -h, --help    Show this help message.\n"
					);
			return 0;
		}
	}
	else
	{
		fprintf(stderr , "Expecting one argument to -separate_output-.\n");
		fprintf(stderr , "Try:\n");
		fprintf(stderr , "   runclustertoll separate_output -h\n");
		fprintf(stderr , "for more details.\n");
		return 1;
	}

	/*
	 * TODO:
	 * - handle command line arguments
	 * - make sure number of starts matches number of ends
	 */

	int buffer_size = 0; // Size of buffer.
	int buffer_index = 0; // Current index in buffer.
												// If buffer size is 3 then this index will take values:
												// 0 1 2 0 1 2 0 1 2 ...
												// in the while loop reading data from stdin.
	char * buffer; // Buffer to hold data read from stdin.

	const char* cluster_begin_data = getenv("CLUSTER_BEGIN_DATA"); // <CLUSTER_BEGIN_DATA>
	
	const char* cluster_end_data = getenv("CLUSTER_END_DATA"); // <CLUSTER_END_DATA>
	
	const char* cluster_begin_bottom = getenv("CLUSTER_BEGIN_BOTTOM"); // <CLUSTER_BEGIN_BOTTOM>
	
	const char* cluster_end_bottom = getenv("CLUSTER_END_BOTTOM"); // <CLUSTER_END_BOTTOM>
	
	const char* cluster_begin_type = getenv("CLUSTER_BEGIN_TYPE"); // <CLUSTER_BEGIN_TYPE>
	
	const char* cluster_end_type = getenv("CLUSTER_END_TYPE"); // <CLUSTER_END_TYPE>

	if(
			NULL == cluster_begin_data || 
			NULL == cluster_end_data ||
			NULL == cluster_begin_bottom ||
			NULL == cluster_end_bottom ||
			NULL == cluster_begin_type ||
			NULL == cluster_end_type
	)
	{
		fprintf(stderr , "Missing environment variables in -separate_output-.\n");
		return 1;
	}

	if(sizeof(char) != 1)
	{
		fprintf(stderr , "Size of -char- in -separate_output- is not 1.\n");
		return 1;
	}

	if(NULL == freopen(NULL, "rb", stdin))
	{
		fprintf(stderr , "Could not reopen stdin in binary mode in -separate_output-.\n");
		return 1;
	}

	int cluster_begin_data_index = 0; // If reaches the same value as cluster_begin_data_length 
																		// then entire <CLUSTER_BEGIN_DATA> has been read.
	int cluster_begin_data_length = strlen(cluster_begin_data); // Length of <CLUSTER_BEGIN_DATA>.
	if(cluster_begin_data_length > buffer_size)
	{
		buffer_size = cluster_begin_data_length;
	}
	
	int cluster_end_data_index = 0; // Similar as above.
	int cluster_end_data_length = strlen(cluster_end_data); // Similar as above.
	if(cluster_end_data_length > buffer_size)
	{
		buffer_size = cluster_end_data_length;
	}
	
	int cluster_begin_bottom_index = 0; // Similar as above.
	int cluster_begin_bottom_length = strlen(cluster_begin_bottom); // Similar as above.
	if(cluster_begin_bottom_length > buffer_size)
	{
		buffer_size = cluster_begin_bottom_length;
	}
	
	int cluster_end_bottom_index = 0; // Similar as above.
	int cluster_end_bottom_length = strlen(cluster_end_bottom); // Similar as above.
	if(cluster_end_bottom_length > buffer_size)
	{
		buffer_size = cluster_end_bottom_length;
	}

	int cluster_begin_type_index = 0; // Similar as above.
	int cluster_begin_type_length = strlen(cluster_begin_type); // Similar as above.
	if(cluster_begin_type_length > buffer_size)
	{
		buffer_size = cluster_begin_type_length;
	}

	int cluster_end_type_index = 0; // Similar as above.
	int cluster_end_type_length = strlen(cluster_end_type); // Similar as above.
	if(cluster_end_type_length > buffer_size)
	{
		buffer_size = cluster_end_type_length;
	}

	buffer = (char *)malloc(buffer_size * sizeof(char));

	int data_number = 0; // Number of data segment (between <CLUSTER_BEGIN_DATA> and <CLUSTER_END_DATA>)
	int type_number = 0; // Number of type segment (between <CLUSTER_BEGIN_TYPE> and <CLUSTER_END_TYPE>)
	int bottom_number = 0; // Number of bottom segment (between <CLUSTER_BEGIN_BOTTOM> and <CLUSTER_END_BOTTOM>)

	int open_data_brackets = 0; // Number of <CLUSTER_BEGIN_DATA>, schould be at most 1. 
															// Nested data segments are not alowed (eg <CLUSTER_BEGIN_DATA><CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA><CLUSTER_END_DATA>).
	int open_type_brackets = 0; // Number of <CLUSTER_BEGIN_TYPE>, schould be at most 1.
															// Nested type segments are not allowed (eg <CLUSTER_BEGIN_TYPE><CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE><CLUSTER_END_TYPE>).
	int open_bottom_brackets = 0; // Similar as above.

	char ch; // Single byte, read from stdin in the following loop.
	long int byte_index = 0; // Index of the byte.

	long int in_cluster_bottom = -1; // Index of the start of a bottom segment.
	long int in_cluster_data = -1; // Index of the start of a data segment.
	long int in_cluster_type = -1; // Index of the start of a type segment.

	char * bottom_file_name;
	char * type_file_name;
	char * data_file_name;
	bottom_file_name = (char *)malloc(sizeof(char) * (100 + strlen(argv[1])));
	type_file_name = (char *)malloc(sizeof(char) * (100 + strlen(argv[1])));
	data_file_name = (char *)malloc(sizeof(char) * (100 + strlen(argv[1])));
	FILE * bottom_file = NULL;
	FILE * type_file = NULL;
	FILE * data_file = NULL;

	sprintf(bottom_file_name , "%s/bottom" , argv[1]);
	bottom_file = fopen(bottom_file_name , "ab");
	if(NULL == bottom_file)
	{
		fprintf(stderr , "Failed to open bottom file in -separate_output-.\n");
		return 1;
	}
	if(0 != fclose(bottom_file))
	{
		fprintf(stderr , "Could not close bottom file in -separate_output-.\n");
		return 1;
	}
	bottom_file = NULL;

	while(1)
	{
		int r = fread(&ch , 1 , 1 , stdin);
		if(0 == r) break;

		/*
		 * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		 * Checking is ending of bottom encountered:
		 */

		if(cluster_end_bottom[cluster_end_bottom_index] == ch)
		{
			cluster_end_bottom_index ++;
			//fprintf(stderr , "%lu : ch , cluster_end_bottom_index = %c , %i\n" , byte_index , ch , bottom_number);
		}
		else
		{
			cluster_end_bottom_index = 0;
		}
	
		if(cluster_end_bottom_index == cluster_end_bottom_length)
		{
			in_cluster_bottom = -1;
			open_bottom_brackets --;
			bottom_number ++;
			if(NULL != bottom_file)
			{
				//fprintf(stderr , "Closing bottom file.\n");
				if(0 != fclose(bottom_file))
				{
					fprintf(stderr , "Could not close bottom file in -separate_output-.\n");
					return 1;
				}
				bottom_file = NULL;
			}
			//fprintf(stderr , "%lu : bottom_number = %i\n" , byte_index , bottom_number);
		}
	
		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */

		/*
		 * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		 * Checking is ending of data encountered:
		 */

		if(cluster_end_data[cluster_end_data_index] == ch)
		{
			cluster_end_data_index ++;
			//fprintf(stderr , "%lu : ch , cluster_end_data_index = %c , %i\n" , byte_index , ch , data_number);
		}
		else
		{
			cluster_end_data_index = 0;
		}
	
		if(cluster_end_data_index == cluster_end_data_length)
		{
			in_cluster_data = -1;
			open_data_brackets --;
			data_number ++;
			if(NULL != data_file)
			{
				//fprintf(stderr , "Closing data file.\n");
				if(0 != fclose(data_file))
				{
					fprintf(stderr , "Could not close data file in -separate_output-.\n");
					return 1;
				}
				data_file = NULL;
			}
			//fprintf(stderr , "%lu : data_number = %i\n" , byte_index , data_number);
		}
	
		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */
		
		/*
		 * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		 * Checking is ending of type encountered:
		 */

		if(cluster_end_type[cluster_end_type_index] == ch)
		{
			cluster_end_type_index ++;
			//fprintf(stderr , "%lu : ch , cluster_end_data_index = %c , %i\n" , byte_index , ch , cluster_end_type_index);
		}
		else
		{
			cluster_end_type_index = 0;
		}
	
		if(cluster_end_type_index == cluster_end_type_length)
		{
			in_cluster_type = -1;
			open_type_brackets --;
			type_number ++;
			if(NULL != type_file)
			{
				if(0 != fclose(type_file))
				{
					fprintf(stderr , "Could not close type file in -separate_output-.\n");
					return 1;
				}
				type_file = NULL;
			}
		}

		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */

		buffer[buffer_index] = ch;

		/*
		 * <<<<<<<<<<<<<
		 * Writing bottom.
		 */

		if(byte_index >= in_cluster_bottom && in_cluster_bottom >= 0)
		{
			if(NULL == bottom_file)
			{
				sprintf(bottom_file_name , "%s/bottom" , argv[1]);
				bottom_file = fopen(bottom_file_name , "ab");
				if(NULL == bottom_file)
				{
					fprintf(stderr , "Failed to open bottom file in -separate_output-.\n");
					return 1;
				}
				// TODO - write number of data segment.
			}
			/* 
			 * Writing data to file.
			 * First check if the number of open data brackets is 1.
			 */
			if(open_bottom_brackets == 1)
			{
				/*
				 * buffer[ind] contains a byte that is part of the data.
				 */
				int ind = (buffer_index - cluster_end_bottom_length + 1) % buffer_size;
				if(ind < 0) ind = buffer_size + ind;
				if(NULL != bottom_file)
				{
					fwrite(buffer + ind , sizeof(char) , 1 , bottom_file);
				}
			}
			else
			{
				fprintf(stderr , "Writing bottom. Output of cluster function has the wrong format.\n");
				fprintf(stderr , "Expecting:\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   ...\n");
				fprintf(stderr , "where <...> contain the contents of environmnet variables\n");
				fprintf(stderr , "set in -runclustertool-.\n");
				return 1;
			}
		}

		/*
		 * >>>>>>>>>>>>>
		 */
		
		/*
		 * <<<<<<<<<<<<<
		 * Writing type.
		 */

		if(byte_index >= in_cluster_type && in_cluster_type >= 0)
		{
			if(NULL == type_file)
			{
				sprintf(type_file_name , "%s/%i_type" , argv[1] , type_number);
				type_file = fopen(type_file_name , "wb");
				if(NULL == type_file)
				{
					fprintf(stderr , "Failed to open type file in -separate_output-.\n");
					return 1;
				}
			}
			/* 
			 * Writing data to file.
			 * First check if the number of open data brackets is 1.
			 */
			if(open_type_brackets == 1)
			{
				/*
				 * buffer[ind] contains a byte that is part of the data.
				 */
				int ind = (buffer_index - cluster_end_type_length + 1) % buffer_size;
				if(ind < 0) ind = buffer_size + ind;
				if(NULL != type_file)
				{
					fwrite(buffer + ind , sizeof(char) , 1 , type_file);
				}
			}
			else
			{
				fprintf(stderr , "Writing type. Output of cluster function has the wrong format.\n");
				fprintf(stderr , "Expecting:\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   ...\n");
				fprintf(stderr , "where <...> contain the contents of environmnet variables\n");
				fprintf(stderr , "set in -runclustertool-.\n");
				return 1;
			}
		}

		/*
		 * >>>>>>>>>>>>>
		 */

		/*
		 * <<<<<<<<<<<<<
		 * Writing data.
		 */

		if(byte_index >= in_cluster_data && in_cluster_data >= 0)
		{
			if(NULL == data_file)
			{
				sprintf(data_file_name , "%s/%i_data" , argv[1] , data_number);
				data_file = fopen(data_file_name , "wb");
				if(NULL == data_file)
				{
					fprintf(stderr , "Failed to open data file in -separate_output-.\n");
					return 1;
				}
			}
			/* 
			 * Writing data to file.
			 * First check if the number of open data brackets is 1.
			 */
			if(open_data_brackets == 1 && data_number + 1 == type_number)
			{
				/*
				 * buffer[ind] contains a byte that is part of the data.
				 */
				int ind = (buffer_index - cluster_end_data_length + 1) % buffer_size;
				if(ind < 0) ind = buffer_size + ind;
				if(NULL != data_file)
				{
					fwrite(buffer + ind , sizeof(char) , 1 , data_file);
				}
				//printf("%c" , buffer[ind]);
			}
			else
			{
				//fprintf(stderr , "%lu : datanumber , type_number =  %i , %i\n" , byte_index , data_number , type_number);
				fprintf(stderr , "Writitng data. Output of cluster function has the wrong format.\n");
				fprintf(stderr , "Expecting:\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_TYPE>...<CLUSTER_END_TYPE>\n");
				fprintf(stderr , "   <CLUSTER_BEGIN_DATA>...<CLUSTER_END_DATA>\n");
				fprintf(stderr , "   ...\n");
				fprintf(stderr , "where <...> contain the contents of environmnet variables\n");
				fprintf(stderr , "set in -runclustertool-.\n");
				return 1;
			}
		}

		/*
		 * >>>>>>>>>>>>>
		 */
	
		/*
		 * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		 * Checking if beginning of bottom encountered:
		 */

		if(cluster_begin_bottom[cluster_begin_bottom_index] == ch)
		{
			cluster_begin_bottom_index ++;
		}
		else
		{
			cluster_begin_bottom_index = 0;
		}
	
		if(cluster_begin_bottom_index == cluster_begin_bottom_length)
		{
			in_cluster_bottom = byte_index + cluster_end_bottom_length;
			open_bottom_brackets ++;
		}

		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */
		/*
		 * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		 * Checking if beginning of data encountered:
		 */

		if(cluster_begin_data[cluster_begin_data_index] == ch)
		{
			cluster_begin_data_index ++;
		}
		else
		{
			cluster_begin_data_index = 0;
		}
	
		if(cluster_begin_data_index == cluster_begin_data_length)
		{
			in_cluster_data = byte_index + cluster_end_data_length;
			open_data_brackets ++;
		}

		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */
	
		/*
		 * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		 * Checking if beginning of type encountered:
		 */

		if(cluster_begin_type[cluster_begin_type_index] == ch)
		{
			cluster_begin_type_index ++;
		}
		else
		{
			cluster_begin_type_index = 0;
		}
	
		if(cluster_begin_type_index == cluster_begin_type_length)
		{
			in_cluster_type = byte_index + cluster_end_type_length;
			open_type_brackets ++;
		}

		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */
	
		buffer_index ++;
		buffer_index = buffer_index % buffer_size;

		byte_index ++;
	}

	free(buffer);
	free(bottom_file_name);
	free(data_file_name);
	free(type_file_name);
	return 0;
}
