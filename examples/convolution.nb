(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27068,        608]
NotebookOptionsPosition[     21623,        540]
NotebookOutlinePosition[     21959,        555]
CellTagsIndexPosition[     21916,        552]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
  "<<", "\"\</home/kacper/GIT_HUB/quick_note/NOTES/1523167151189_4f290bdc-\
7f6d-4484-8450-41707feaa861/FILES/clev/wolframlanguage/Clev.wl\>\""}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.7565521277825727`*^9, 3.7565521363878517`*^9}, {
  3.7565521788042383`*^9, 3.75655219896313*^9}, {3.7566202938247423`*^9, 
  3.756620322444303*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"3a98cf07-260c-4901-bcfd-5d28cc9751f3"],

Cell[BoxData[
 RowBox[{"Dynamic", "[", 
  RowBox[{"getStatus", "[", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7565357060036793`*^9, 3.756535712697895*^9}, {
   3.756552018214802*^9, 3.756552019564389*^9}, 3.763444056026855*^9},
 CellLabel->"In[2]:=",ExpressionUUID->"22903225-8ccb-41a4-a97a-f356c840d757"],

Cell[BoxData[
 RowBox[{
  RowBox[{"initializeClev", "[", 
   RowBox[{
   "\"\</home/kacper/GIT_HUB/quick_note/NOTES/1523167151189_4f290bdc-7f6d-\
4484-8450-41707feaa861/FILES/clev/runclevtool\>\"", " ", ",", " ", 
    "\"\</home/kacper/GIT_HUB/quick_note/NOTES/1523167151189_4f290bdc-7f6d-\
4484-8450-41707feaa861/FILES/clev_examples/example_convolution\>\"", " ", ",",
     " ", 
    RowBox[{"\"\<Queue\>\"", "\[Rule]", "True"}]}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.756526039419086*^9, 3.756526067465906*^9}, {
  3.756526097888672*^9, 3.75652615063269*^9}, {3.756620262082391*^9, 
  3.756620285139516*^9}, {3.75800987481069*^9, 3.758009879657345*^9}, {
  3.7580101164408417`*^9, 3.758010116968081*^9}, {3.7582647435263453`*^9, 
  3.758264752765533*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"7f8f0387-edff-4940-8020-05525448efe7"],

Cell[BoxData[
 RowBox[{"a", " ", "=", " ", 
  RowBox[{
   RowBox[{"rawType", "[", "\"\<number\>\"", "]"}], "[", 
   RowBox[{"\"\<6\>\"", " ", ",", " ", 
    RowBox[{"FormatType", "\[Rule]", "TextForm"}], " ", ",", " ", 
    RowBox[{"PageWidth", "\[Rule]", "Infinity"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.756461962242505*^9, 3.756461966586206*^9}, {
  3.756462800956807*^9, 3.756462805267748*^9}, {3.75646489008707*^9, 
  3.756464910190647*^9}, {3.7564651502020817`*^9, 3.75646515084381*^9}, {
  3.756526349352579*^9, 3.75652636032014*^9}, {3.756526783055129*^9, 
  3.756526783950376*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"0f2202d6-09d2-4c9b-93fd-6fe0293bf4f2"],

Cell[BoxData[
 RowBox[{"b", " ", "=", " ", 
  RowBox[{
   RowBox[{"rawType", "[", "\"\<image\>\"", "]"}], "[", 
   RowBox[{
   "ReadByteArray", "[", "\"\</home/kacper/Pictures/DkiUi7Y.jpg\>\"", "]"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.756530265003632*^9, 3.756530282051453*^9}, {
   3.756530318451344*^9, 3.756530333035891*^9}, 3.7565304420367947`*^9},
 CellLabel->"In[5]:=",ExpressionUUID->"1e8447c6-c4de-4127-8ddd-78d187866de5"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"bad1", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<negate\>\"", "]"}], "[", 
    RowBox[{"a", " ", ",", " ", "b"}], "]"}]}], "*)"}]], "Input",
 CellChangeTimes->{{3.763444119412953*^9, 3.763444133057509*^9}, {
  3.763444324371929*^9, 3.763444325071567*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"ebbc8331-9086-4348-9589-72b40a68b962"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"bad2", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<negate\>\"", "]"}], "[", "bad1", "]"}]}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.763444208595971*^9, 3.763444224473709*^9}, {
  3.763444328450595*^9, 3.7634443291594887`*^9}},
 CellLabel->"In[7]:=",ExpressionUUID->"f5552282-1ffa-49f7-8f31-6cae89d94222"],

Cell[BoxData[
 RowBox[{"runLoop", "[", "]"}]], "Input",
 CellChangeTimes->{{3.763444238315098*^9, 3.763444239688076*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"a874668a-9671-4379-be6d-9a7045ad7378"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"c", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<blur\>\"", "]"}], "[", 
    RowBox[{"a", " ", ",", " ", "b"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"d", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<blur\>\"", "]"}], "[", 
    RowBox[{"a", " ", ",", " ", "c"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"e", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<blur\>\"", "]"}], "[", 
    RowBox[{"a", " ", ",", " ", "d"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"f", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<blur\>\"", "]"}], "[", 
    RowBox[{"a", " ", ",", " ", "e"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"g", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<blur\>\"", "]"}], "[", 
    RowBox[{"a", " ", ",", " ", "f"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.756546795589409*^9, 3.756546810086442*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"f1fadba4-b42e-43f4-a7c3-5cab51589585"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"clearData", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.763270580976753*^9, 3.7632705950613956`*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"b751c915-3a97-4577-8da6-5a8501b09fdd"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.756532139136519*^9, 3.75653214232787*^9}, {
  3.756547575769373*^9, 3.7565475764558353`*^9}, {3.7565523959867907`*^9, 
  3.756552396833959*^9}, {3.756622643432506*^9, 3.756622644070971*^9}, {
  3.757150568850917*^9, 3.757150569010107*^9}, {3.7577425532816143`*^9, 
  3.757742555904759*^9}, {3.7580098905079727`*^9, 3.758009891289332*^9}},
 CellLabel->"In[15]:=",ExpressionUUID->"25f790a1-abaf-4fc0-a4ad-81a81bd9b55f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"bluredlist", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{
    "c", " ", ",", " ", "d", " ", ",", " ", "e", " ", ",", " ", "f", " ", ",",
      " ", "g"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7565468317906523`*^9, 3.756546841052331*^9}, 
   3.757742601785265*^9, 3.7577426498339043`*^9},
 CellLabel->"In[16]:=",ExpressionUUID->"a0827187-25c0-4b37-a201-d69d5fefa6b3"],

Cell[BoxData["bluredlist"], "Input",
 CellLabel->"In[17]:=",ExpressionUUID->"e72ce318-7b24-4526-af4d-d5d4f5a21330"],

Cell[BoxData[
 RowBox[{
  RowBox[{"newblured", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"funApp", "[", "\"\<blur\>\"", "]"}], "[", 
      RowBox[{"a", " ", ",", " ", "#"}], "]"}], "&"}], "/@", "bluredlist"}]}],
   ";"}]], "Input",
 CellChangeTimes->{{3.756546849781374*^9, 3.7565468783083*^9}, {
  3.756546965021324*^9, 3.756546967507744*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"dfcd6ffe-3cb9-4c7e-a628-5a518bdcc9a2"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.758009896834147*^9, 3.758009897401696*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"64378b79-0910-42aa-8c3f-df1fd213a1cb"],

Cell[BoxData["newblured"], "Input",
 CellLabel->"In[20]:=",ExpressionUUID->"5134bd05-7cf3-4029-af0f-e7b45fabfa3c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"negated", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"funApp", "[", "\"\<negate\>\"", "]"}], "[", "#", "]"}], "&"}], 
    "/@", "bluredlist"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7566213273118563`*^9, 3.7566213376394367`*^9}},
 CellLabel->"In[21]:=",ExpressionUUID->"caa36ca0-320d-47ff-a391-ce265cc4e638"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.758009900915082*^9, 3.758009901507533*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"606c083d-004c-4e69-85fa-cfa54156b904"],

Cell[BoxData["negated"], "Input",
 CellChangeTimes->{{3.757062304422332*^9, 3.757062306373209*^9}},
 CellLabel->"In[23]:=",ExpressionUUID->"92074441-be09-4026-b6fc-55564bb7dd14"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.75654743727944*^9, 3.756547440121031*^9}, {
  3.7565475843691998`*^9, 3.756547584991748*^9}},
 CellLabel->"In[24]:=",ExpressionUUID->"086bc584-652f-4faa-b07d-f1d2b9cf00ea"],

Cell[BoxData[
 RowBox[{"Partition", "[", 
  RowBox[{
   RowBox[{"Riffle", "[", 
    RowBox[{"bluredlist", "  ", ",", " ", "negated"}], "]"}], " ", ",", " ", 
   "2"}], "]"}]], "Input",
 CellLabel->"In[25]:=",ExpressionUUID->"422245c2-3f81-422d-8c99-fa78f57f2dfb"],

Cell[BoxData[
 RowBox[{
  RowBox[{"sum", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"funApp", "[", "\"\<add\>\"", "]"}], "@@", "#"}], ")"}], "&"}],
     "/@", 
    RowBox[{"Partition", "[", 
     RowBox[{
      RowBox[{"Riffle", "[", 
       RowBox[{"bluredlist", "  ", ",", " ", "negated"}], "]"}], " ", ",", 
      " ", "2"}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7565472131239653`*^9, 3.7565472437939873`*^9}, {
   3.7565475373403463`*^9, 3.756547547840088*^9}, {3.756547658145092*^9, 
   3.756547658831324*^9}, 3.756621495386466*^9},
 CellLabel->"In[26]:=",ExpressionUUID->"617c2475-3e2d-4a6a-98f0-439ce2514173"],

Cell[BoxData["sum"], "Input",
 CellChangeTimes->{{3.757062366925349*^9, 3.757062367247549*^9}},
 CellLabel->"In[27]:=",ExpressionUUID->"4b50bb22-6976-4414-b083-27c048b64b80"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.756542829711207*^9, 3.756542831414176*^9}, {
  3.7566226393765574`*^9, 3.756622640286955*^9}, {3.757742841631434*^9, 
  3.757742843470518*^9}, {3.758009906330196*^9, 3.7580099069853077`*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"d6876769-1c74-4773-9fb2-df8c96dda406"],

Cell[BoxData[
 RowBox[{"h", " ", "=", " ", 
  RowBox[{
   RowBox[{"rawType", "[", "\"\<kernel\>\"", "]"}], "[", 
   RowBox[{
   "\"\<3x3: 1.0,3.0,1.0 0.0,0.0,0.0 -1.0,-3.0,-1.0\>\"", " ", ",", " ", 
    RowBox[{"FormatType", "\[Rule]", "TextForm"}], " ", ",", " ", 
    RowBox[{"PageWidth", "\[Rule]", "Infinity"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7566219311475983`*^9, 3.7566219390998583`*^9}, {
   3.756622006147143*^9, 3.7566220449632187`*^9}, 3.756622473896524*^9},
 CellLabel->"In[29]:=",ExpressionUUID->"7395c608-f756-4f49-b0e5-53d76ce29766"],

Cell[BoxData[
 RowBox[{
  RowBox[{"convoluted", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"funApp", "[", "\"\<convolve\>\"", "]"}], "[", 
      RowBox[{"h", " ", ",", " ", "#"}], "]"}], "&"}], "/@", "sum"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.7566225134319153`*^9, 3.7566225273120317`*^9}, {
  3.7566234980278797`*^9, 3.756623498273631*^9}},
 CellLabel->"In[30]:=",ExpressionUUID->"5011cd05-aa82-4c03-81ba-113007780465"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.758264763405703*^9, 3.758264763941298*^9}},
 CellLabel->"In[31]:=",ExpressionUUID->"d66d367a-7296-4695-aba1-a63bf6d688c4"],

Cell[BoxData["convoluted"], "Input",
 CellLabel->"In[32]:=",ExpressionUUID->"2bd10a9e-4267-48cf-9f4d-a1ebdc33726c"],

Cell[BoxData["c"], "Input",
 CellLabel->"In[33]:=",ExpressionUUID->"a5626bb9-40d4-4f1d-9df6-b3c927e6eb60"],

Cell[BoxData["h"], "Input",
 CellChangeTimes->{3.757062597757434*^9},
 CellLabel->"In[34]:=",ExpressionUUID->"8c412e79-240b-42e1-a881-bdd426acabed"],

Cell[BoxData[
 RowBox[{"i", " ", "=", " ", 
  RowBox[{
   RowBox[{"funApp", "[", "\"\<modify\>\"", "]"}], "[", 
   RowBox[{"h", ",", "c"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7566252738690166`*^9, 3.75662531403514*^9}, 
   3.75662542995611*^9},
 CellLabel->"In[35]:=",ExpressionUUID->"987b1cb0-e7b8-4fa3-8c0a-6dc893a9c0a2"],

Cell[BoxData["a"], "Input",
 CellChangeTimes->{3.75706266181205*^9},
 CellLabel->"In[36]:=",ExpressionUUID->"dacf9fed-bf5a-4879-b894-44d3f1738f47"],

Cell[BoxData[
 RowBox[{"j", " ", "=", " ", 
  RowBox[{
   RowBox[{"funApp", "[", "\"\<modify\>\"", "]"}], "[", 
   RowBox[{"a", ",", "c"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7566253409679203`*^9, 3.7566253434429407`*^9}, 
   3.756625431187948*^9},
 CellLabel->"In[37]:=",ExpressionUUID->"f9b70b6d-4a44-4d8f-8aff-7739d2e266ca"],

Cell[BoxData[
 RowBox[{"row", " ", "=", " ", 
  RowBox[{
   RowBox[{"binApp", "[", "\"\<add\>\"", "]"}], "[", 
   RowBox[{"b", " ", ",", "c", " ", ",", " ", "d", " ", ",", " ", "e"}], " ", 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.75663322759133*^9, 3.756633279899551*^9}, 
   3.7566338781534433`*^9, 3.756634353130247*^9, 3.756635312990443*^9, 
   3.7566354109837017`*^9},
 CellLabel->"In[38]:=",ExpressionUUID->"6eab82c2-cc92-4d9e-95e2-fe91faff7d30"],

Cell[BoxData[
 RowBox[{"k", " ", "=", " ", 
  RowBox[{
   RowBox[{"rawType", "[", "\"\<kernel\>\"", "]"}], "[", 
   RowBox[{"\"\<3x3: 0.0,0.0,0.0 0.0,1.0,0.0 0.0,0.0,-1.0\>\"", " ", ",", " ", 
    RowBox[{"FormatType", "\[Rule]", "TextForm"}], " ", ",", " ", 
    RowBox[{"PageWidth", "\[Rule]", "Infinity"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7566361709581413`*^9, 3.756636202283476*^9}, 
   3.756636253388564*^9},
 CellLabel->"In[39]:=",ExpressionUUID->"85a0f51f-0b48-4001-afa0-a6fdcafaef82"],

Cell[BoxData["b"], "Input",
 CellChangeTimes->{3.757062761091465*^9},
 CellLabel->"In[40]:=",ExpressionUUID->"f753ef57-ebfa-4fd6-8404-75046b3409a7"],

Cell[BoxData[
 RowBox[{"l", " ", "=", " ", 
  RowBox[{
   RowBox[{"funApp", "[", "\"\<convolve\>\"", "]"}], "[", 
   RowBox[{"k", " ", ",", " ", "b"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7566362328064337`*^9, 3.756636244324952*^9}},
 CellLabel->"In[41]:=",ExpressionUUID->"24ffb4ae-bd12-4ad2-b96e-ce229d6f7173"],

Cell[BoxData[
 RowBox[{"m", " ", "=", " ", 
  RowBox[{
   RowBox[{"rawType", "[", "\"\<image\>\"", "]"}], "[", 
   RowBox[{
   "ReadByteArray", "[", "\"\</home/kacper/Pictures/EcyVK7o.jpg\>\"", "]"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.7571533748127413`*^9, 3.7571533989711313`*^9}, {
  3.7571534433308287`*^9, 3.7571534518348503`*^9}},
 CellLabel->"In[42]:=",ExpressionUUID->"44d21148-88bc-4cf5-984d-9809b46e859f"],

Cell[BoxData[
 RowBox[{"n", " ", "=", " ", 
  RowBox[{
   RowBox[{"rawType", "[", "\"\<image\>\"", "]"}], "[", 
   RowBox[{
   "ReadByteArray", "[", "\"\</home/kacper/Pictures/ecYx7wi.jpg\>\"", "]"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.757153455772407*^9, 3.7571534901068563`*^9}},
 CellLabel->"In[43]:=",ExpressionUUID->"fb022689-69df-4fb5-a659-deb2fee8b59c"],

Cell[BoxData[
 RowBox[{"o", " ", "=", " ", 
  RowBox[{
   RowBox[{"funApp", "[", "\"\<reverse\>\"", "]"}], "[", 
   RowBox[{"m", " ", ",", " ", "n"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.757153499036189*^9, 3.757153517186687*^9}},
 CellLabel->"In[44]:=",ExpressionUUID->"50d0fe31-745e-4275-b922-14b79e0ae070"],

Cell[BoxData[
 RowBox[{"runLoop", "[", "]"}]], "Input",
 CellChangeTimes->{{3.757153520547453*^9, 3.7571535222024393`*^9}},
 CellLabel->"In[45]:=",ExpressionUUID->"2acaf77e-8f5e-4523-8340-ce9c36d80db1"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"getStatus", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7580099381386747`*^9, 3.758009939735613*^9}, {
  3.7582647742135057`*^9, 3.7582647749571533`*^9}},
 CellLabel->"In[46]:=",ExpressionUUID->"b7f5cc26-3179-41b0-8964-a203d1a922b6"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"clearData", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7566226637195787`*^9, 3.756622665343801*^9}, {
   3.756622890558207*^9, 3.756622891877263*^9}, {3.756623107324683*^9, 
   3.756623108019916*^9}, {3.756717613993384*^9, 3.7567176151523027`*^9}, 
   3.756718514407823*^9, {3.757153624403331*^9, 3.757153626361871*^9}, {
   3.758264775894244*^9, 3.758264776509253*^9}},
 CellLabel->"In[47]:=",ExpressionUUID->"c296155f-a38d-4c15-9da7-6e3b7bd9edb4"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"initializeClev", "[", 
   RowBox[{
   "\"\</home/kacper/GIT_HUB/quick_note/NOTES/1523167151189_4f290bdc-7f6d-\
4484-8450-41707feaa861/FILES/clev/runclevtool\>\"", " ", ",", " ", 
    "\"\</home/kacper/GIT_HUB/quick_note/NOTES/1523167151189_4f290bdc-7f6d-\
4484-8450-41707feaa861/FILES/clev_examples/example_convolution\>\""}], "]"}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.758264777166629*^9, 3.7582647790610447`*^9}},
 CellLabel->"In[48]:=",ExpressionUUID->"a9ff9293-394b-4bcb-b380-d5829101e835"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"okimage", " ", "=", " ", 
   RowBox[{
    RowBox[{"rawType", "[", "\"\<image\>\"", "]"}], "[", 
    RowBox[{
    "ReadByteArray", "[", "\"\</home/kacper/Pictures/DkiUi7Y.jpg\>\"", "]"}], 
    "]"}]}], "*)"}]], "Input",
 CellChangeTimes->{{3.756717910025578*^9, 3.756717935537303*^9}, {
  3.758264779773674*^9, 3.7582647807571373`*^9}},
 CellLabel->"In[49]:=",ExpressionUUID->"9c9d5351-50ba-4fc0-a282-3fd9486cf897"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{
   "inspect", " ", "this", " ", "and", " ", "other", " ", "special", " ", 
    RowBox[{"cases", ":"}]}], "\[IndentingNewLine]", "\[Rule]", 
   "\[IndentingNewLine]", 
   RowBox[{"image", " ", "\[Rule]", " ", "\[IndentingNewLine]", 
    RowBox[{"\[Rule]", " ", 
     RowBox[{"image", "\[IndentingNewLine]", "..."}]}]}]}], "*)"}]], "Input",
 CellChangeTimes->{{3.757157567343652*^9, 3.757157592469842*^9}},
 CellLabel->"In[50]:=",ExpressionUUID->"d9196f75-3f57-48ac-aea9-97e166846200"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"p", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<sink\>\"", "]"}], "[", "okimage", "]"}]}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.757157307751834*^9, 3.757157319222579*^9}, {
  3.75715747475391*^9, 3.757157476329151*^9}, {3.75826478264569*^9, 
  3.7582647831970043`*^9}},
 CellLabel->"In[51]:=",ExpressionUUID->"1bf3f823-58f8-4d42-adfe-427ae4d25d25"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7571573221352377`*^9, 3.757157323744318*^9}, {
  3.758264783933462*^9, 3.7582647845249777`*^9}},
 CellLabel->"In[52]:=",ExpressionUUID->"154747ec-2ad2-4c8f-8f6a-9fa8c78fca6b"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"wrongkernel", " ", "=", " ", 
   RowBox[{
    RowBox[{"rawType", "[", "\"\<kernel\>\"", "]"}], "[", 
    RowBox[{
    "\"\<3x3: p.0,0.0,0.0 0.0,1.0,0.0 0.0,0.0,-1.0\>\"", " ", ",", " ", 
     RowBox[{"FormatType", "\[Rule]", "TextForm"}], " ", ",", " ", 
     RowBox[{"PageWidth", "\[Rule]", "Infinity"}]}], "]"}]}], "*)"}]], "Input",\

 CellChangeTimes->{{3.756717626704509*^9, 3.756717633921067*^9}, {
   3.756717914897642*^9, 3.756717915736724*^9}, 3.756718583856798*^9, {
   3.758264785277752*^9, 3.758264786244988*^9}},
 CellLabel->"In[53]:=",ExpressionUUID->"f0b3f357-80ff-4117-bc3d-b5b281cf2b98"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"wrongb", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<convolve\>\"", "]"}], "[", 
    RowBox[{"okimage", " ", ",", " ", "wrongkernel"}], "]"}]}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.7567177336820183`*^9, 3.756717756929175*^9}, 
   3.7567178360437403`*^9, {3.756717947930311*^9, 3.7567179502661533`*^9}, 
   3.756718934210985*^9, 3.756719045779236*^9, {3.7567191710646887`*^9, 
   3.756719172271139*^9}, 3.7567198772890377`*^9, 3.756720315054751*^9, 
   3.756720403878644*^9, {3.7582647869336023`*^9, 3.758264787532929*^9}},
 CellLabel->"In[54]:=",ExpressionUUID->"c0a62132-720a-4bd5-8a97-953746fa3d51"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"wrongc", " ", "=", " ", 
   RowBox[{
    RowBox[{"funApp", "[", "\"\<convolve\>\"", "]"}], "[", 
    RowBox[{"wrongkernel", " ", ",", " ", "wrongb"}], "]"}]}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.7567180658735723`*^9, 3.756718067841168*^9}, {
  3.758264788270321*^9, 3.758264788925023*^9}},
 CellLabel->"In[55]:=",ExpressionUUID->"c6eabc7f-a2b4-42ae-940e-1b57f68828ed"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"runLoop", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7567176611624823`*^9, 3.7567176635769243`*^9}, {
  3.758264789645482*^9, 3.75826479055692*^9}},
 CellLabel->"In[56]:=",ExpressionUUID->"dde7bb10-e869-47e7-98cc-21b610211171"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"getStatus", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.756717664929968*^9, 3.756717667912882*^9}, {
  3.758264792013988*^9, 3.7582647928850737`*^9}},
 CellLabel->"In[57]:=",ExpressionUUID->"2c49d19d-0d8d-4b30-b9aa-fc33d6452073"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"allJobs", "[", 
    RowBox[{"wrongb", "[", 
     RowBox[{"[", "1", "]"}], "]"}], "]"}], "[", 
   RowBox[{"[", "2", "]"}], "]"}], "*)"}]], "Input",
 CellChangeTimes->{
  3.756718980488092*^9, {3.756719063633449*^9, 3.756719077066856*^9}, 
   3.756719187490779*^9, 3.7567194472582073`*^9, 3.756719573624282*^9, 
   3.756720100023899*^9, {3.7567202420724154`*^9, 3.7567202833922653`*^9}, {
   3.756720324832261*^9, 3.756720325683921*^9}, {3.758264795445526*^9, 
   3.758264798149025*^9}},
 CellLabel->"In[58]:=",ExpressionUUID->"f02b47fc-bd26-4c4a-a789-2e0546ad172f"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"clearData", "[", "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7567177169059362`*^9, 3.756717718248876*^9}, 
   3.757063237049962*^9, {3.758264798910075*^9, 3.758264799572989*^9}},
 CellLabel->"In[59]:=",ExpressionUUID->"578c4646-acc9-4c20-9e7e-2a029545de1d"],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.7643056579898777`*^9},
 CellLabel->"In[60]:=",ExpressionUUID->"76878975-ecde-4eba-a1c3-84c72506c999"]
},
WindowSize->{683, 768},
WindowMargins->{{Automatic, -1366}, {Automatic, 0}},
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 450, 9, 78, "Input",ExpressionUUID->"3a98cf07-260c-4901-bcfd-5d28cc9751f3"],
Cell[1011, 31, 309, 5, 31, "Input",ExpressionUUID->"22903225-8ccb-41a4-a97a-f356c840d757"],
Cell[1323, 38, 844, 15, 147, "Input",ExpressionUUID->"7f8f0387-edff-4940-8020-05525448efe7"],
Cell[2170, 55, 675, 12, 55, "Input",ExpressionUUID->"0f2202d6-09d2-4c9b-93fd-6fe0293bf4f2"],
Cell[2848, 69, 441, 9, 55, "Input",ExpressionUUID->"1e8447c6-c4de-4127-8ddd-78d187866de5"],
Cell[3292, 80, 392, 8, 31, "Input",ExpressionUUID->"ebbc8331-9086-4348-9589-72b40a68b962"],
Cell[3687, 90, 365, 8, 31, "Input",ExpressionUUID->"f5552282-1ffa-49f7-8f31-6cae89d94222"],
Cell[4055, 100, 199, 3, 31, "Input",ExpressionUUID->"a874668a-9671-4379-be6d-9a7045ad7378"],
Cell[4257, 105, 1097, 31, 124, "Input",ExpressionUUID->"f1fadba4-b42e-43f4-a7c3-5cab51589585"],
Cell[5357, 138, 229, 4, 31, "Input",ExpressionUUID->"b751c915-3a97-4577-8da6-5a8501b09fdd"],
Cell[5589, 144, 520, 8, 31, "Input",ExpressionUUID->"25f790a1-abaf-4fc0-a4ad-81a81bd9b55f"],
Cell[6112, 154, 413, 9, 31, "Input",ExpressionUUID->"a0827187-25c0-4b37-a201-d69d5fefa6b3"],
Cell[6528, 165, 115, 1, 31, "Input",ExpressionUUID->"e72ce318-7b24-4526-af4d-d5d4f5a21330"],
Cell[6646, 168, 450, 11, 31, "Input",ExpressionUUID->"dfcd6ffe-3cb9-4c7e-a628-5a518bdcc9a2"],
Cell[7099, 181, 225, 4, 31, "Input",ExpressionUUID->"64378b79-0910-42aa-8c3f-df1fd213a1cb"],
Cell[7327, 187, 114, 1, 31, "Input",ExpressionUUID->"5134bd05-7cf3-4029-af0f-e7b45fabfa3c"],
Cell[7444, 190, 372, 9, 31, "Input",ExpressionUUID->"caa36ca0-320d-47ff-a391-ce265cc4e638"],
Cell[7819, 201, 225, 4, 31, "Input",ExpressionUUID->"606c083d-004c-4e69-85fa-cfa54156b904"],
Cell[8047, 207, 178, 2, 31, "Input",ExpressionUUID->"92074441-be09-4026-b6fc-55564bb7dd14"],
Cell[8228, 211, 275, 5, 31, "Input",ExpressionUUID->"086bc584-652f-4faa-b07d-f1d2b9cf00ea"],
Cell[8506, 218, 263, 6, 31, "Input",ExpressionUUID->"422245c2-3f81-422d-8c99-fa78f57f2dfb"],
Cell[8772, 226, 676, 17, 55, "Input",ExpressionUUID->"617c2475-3e2d-4a6a-98f0-439ce2514173"],
Cell[9451, 245, 174, 2, 31, "Input",ExpressionUUID->"4b50bb22-6976-4414-b083-27c048b64b80"],
Cell[9628, 249, 373, 6, 31, "Input",ExpressionUUID->"d6876769-1c74-4773-9fb2-df8c96dda406"],
Cell[10004, 257, 559, 10, 78, "Input",ExpressionUUID->"7395c608-f756-4f49-b0e5-53d76ce29766"],
Cell[10566, 269, 456, 11, 31, "Input",ExpressionUUID->"5011cd05-aa82-4c03-81ba-113007780465"],
Cell[11025, 282, 225, 4, 31, "Input",ExpressionUUID->"d66d367a-7296-4695-aba1-a63bf6d688c4"],
Cell[11253, 288, 115, 1, 31, "Input",ExpressionUUID->"2bd10a9e-4267-48cf-9f4d-a1ebdc33726c"],
Cell[11371, 291, 106, 1, 31, "Input",ExpressionUUID->"a5626bb9-40d4-4f1d-9df6-b3c927e6eb60"],
Cell[11480, 294, 148, 2, 31, "Input",ExpressionUUID->"8c412e79-240b-42e1-a881-bdd426acabed"],
Cell[11631, 298, 329, 7, 31, "Input",ExpressionUUID->"987b1cb0-e7b8-4fa3-8c0a-6dc893a9c0a2"],
Cell[11963, 307, 147, 2, 31, "Input",ExpressionUUID->"dacf9fed-bf5a-4879-b894-44d3f1738f47"],
Cell[12113, 311, 333, 7, 31, "Input",ExpressionUUID->"f9b70b6d-4a44-4d8f-8aff-7739d2e266ca"],
Cell[12449, 320, 455, 9, 31, "Input",ExpressionUUID->"6eab82c2-cc92-4d9e-95e2-fe91faff7d30"],
Cell[12907, 331, 503, 9, 55, "Input",ExpressionUUID->"85a0f51f-0b48-4001-afa0-a6fdcafaef82"],
Cell[13413, 342, 148, 2, 31, "Input",ExpressionUUID->"f753ef57-ebfa-4fd6-8404-75046b3409a7"],
Cell[13564, 346, 317, 6, 31, "Input",ExpressionUUID->"24ffb4ae-bd12-4ad2-b96e-ce229d6f7173"],
Cell[13884, 354, 425, 9, 55, "Input",ExpressionUUID->"44d21148-88bc-4cf5-984d-9809b46e859f"],
Cell[14312, 365, 370, 8, 55, "Input",ExpressionUUID->"fb022689-69df-4fb5-a659-deb2fee8b59c"],
Cell[14685, 375, 314, 6, 31, "Input",ExpressionUUID->"50d0fe31-745e-4275-b922-14b79e0ae070"],
Cell[15002, 383, 202, 3, 31, "Input",ExpressionUUID->"2acaf77e-8f5e-4523-8340-ce9c36d80db1"],
Cell[15207, 388, 282, 5, 31, "Input",ExpressionUUID->"b7f5cc26-3179-41b0-8964-a203d1a922b6"],
Cell[15492, 395, 499, 8, 31, "Input",ExpressionUUID->"c296155f-a38d-4c15-9da7-6e3b7bd9edb4"],
Cell[15994, 405, 542, 10, 147, "Input",ExpressionUUID->"a9ff9293-394b-4bcb-b380-d5829101e835"],
Cell[16539, 417, 455, 10, 78, "Input",ExpressionUUID->"9c9d5351-50ba-4fc0-a282-3fd9486cf897"],
Cell[16997, 429, 536, 11, 124, "Input",ExpressionUUID->"d9196f75-3f57-48ac-aea9-97e166846200"],
Cell[17536, 442, 411, 9, 31, "Input",ExpressionUUID->"1bf3f823-58f8-4d42-adfe-427ae4d25d25"],
Cell[17950, 453, 278, 5, 31, "Input",ExpressionUUID->"154747ec-2ad2-4c8f-8f6a-9fa8c78fca6b"],
Cell[18231, 460, 644, 13, 78, "Input",ExpressionUUID->"f0b3f357-80ff-4117-bc3d-b5b281cf2b98"],
Cell[18878, 475, 665, 12, 31, "Input",ExpressionUUID->"c0a62132-720a-4bd5-8a97-953746fa3d51"],
Cell[19546, 489, 417, 9, 31, "Input",ExpressionUUID->"c6eabc7f-a2b4-42ae-940e-1b57f68828ed"],
Cell[19966, 500, 277, 5, 31, "Input",ExpressionUUID->"dde7bb10-e869-47e7-98cc-21b610211171"],
Cell[20246, 507, 278, 5, 31, "Input",ExpressionUUID->"2c49d19d-0d8d-4b30-b9aa-fc33d6452073"],
Cell[20527, 514, 617, 13, 31, "Input",ExpressionUUID->"f02b47fc-bd26-4c4a-a789-2e0546ad172f"],
Cell[21147, 529, 301, 5, 31, "Input",ExpressionUUID->"578c4646-acc9-4c20-9e7e-2a029545de1d"],
Cell[21451, 536, 168, 2, 55, "Input",ExpressionUUID->"76878975-ecde-4eba-a1c3-84c72506c999"]
}
]
*)

