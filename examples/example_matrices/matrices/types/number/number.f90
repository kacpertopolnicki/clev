program matrix
	implicit none
	character * 2000 :: arg
	character * 2000 :: envvalue

	double precision :: nreal , nimag

	integer :: numarg

	numarg = iargc()

	if(numarg .ne. 1) then
		call exit(1)
	end if
	call getarg(1 , arg)

	open(11 , file = trim(arg) , status = 'old')
		read(11 , *) nreal , nimag
	close(11)

	call getenv("CLUSTER_OK" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)
end program matrix
