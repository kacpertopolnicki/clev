program mm
	implicit none
	character * 2000 :: arg
	character * 2000 :: envvalue

	double precision , allocatable :: breal(: , :) , bimag(: , :)
	double precision , allocatable :: creal(: , :) , cimag(: , :)
	integer :: bsize , csize
	double complex , allocatable :: bmat(: , :)
	double complex , allocatable :: cmat(: , :)
	double complex , allocatable :: rmat(: , :)

	integer :: numarg

	numarg = iargc()

	if(numarg .ne. 2) then
		call exit(1)
	end if

	call getarg(1 , arg)
	open(11 , file = trim(arg) , status = "old")
		read(11 , *) bsize
		allocate(breal(bsize , bsize))
		allocate(bimag(bsize , bsize))
		read(11 , *) breal
		read(11 , *) bimag
	close(11)

	call getarg(2 , arg)
	open(11 , file = trim(arg) , status = "old")
		read(11 , *) csize
		allocate(creal(csize , csize))
		allocate(cimag(csize , csize))
		read(11 , *) creal
		read(11 , *) cimag
	close(11)

	if(bsize .ne. csize) then
		call exit(1)
	endif

	allocate(bmat(bsize , bsize))
	allocate(cmat(csize , csize))
	allocate(rmat(csize , csize))

	bmat = breal + (0.0D0 , 1.0D0) * bimag
	cmat = creal + (0.0D0 , 1.0D0) * cimag
	rmat = matmul(bmat , cmat)

	call getenv("CLUSTER_BEGIN_TYPE" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	print * , "matrix"

	call getenv("CLUSTER_END_TYPE" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	call getenv("CLUSTER_BEGIN_DATA" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	print * , bsize
	print * , dble(rmat)
	print * , dimag(rmat)

	call getenv("CLUSTER_END_DATA" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

end program mm
