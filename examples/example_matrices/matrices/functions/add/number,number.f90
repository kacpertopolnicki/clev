program nn
	implicit none
	character * 2000 :: arg
	character * 2000 :: envvalue

	double precision :: breal , bimag
	double precision :: creal , cimag

	integer :: numarg

	numarg = iargc()

	if(numarg .ne. 2) then
		call exit(1)
	end if

	call getarg(1 , arg)
	open(11 , file = trim(arg) , status = "old")
		read(11 , *) breal , bimag
	close(11)

	call getarg(2 , arg)
	open(11 , file = trim(arg) , status = "old")
		read(11 , *) creal , cimag
	close(11)

	call getenv("CLUSTER_BEGIN_TYPE" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	print * , "number"

	call getenv("CLUSTER_END_TYPE" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	call getenv("CLUSTER_BEGIN_DATA" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	print * , breal + creal , bimag + cimag

	call getenv("CLUSTER_END_DATA" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

end program nn
