program mm
	implicit none
	character * 2000 :: arg
	character * 2000 :: envvalue

	double precision , allocatable :: breal(: , :) , bimag(: , :)
	double complex , allocatable :: bmat(: , :)
	integer :: bsize
	double precision :: creal , cimag
	double complex :: cmat
	double complex , allocatable :: rmat(: , :)

	integer :: ii

	integer :: numarg

	numarg = iargc()

	if(numarg .ne. 2) then
		call exit(1)
	end if

	call getarg(2 , arg)
	open(11 , file = trim(arg) , status = "old")
		read(11 , *) bsize
		allocate(breal(bsize , bsize))
		allocate(bimag(bsize , bsize))
		read(11 , *) breal
		read(11 , *) bimag
	close(11)

	call getarg(1 , arg)
	open(11 , file = trim(arg) , status = "old")
		read(11 , *) creal , cimag
	close(11)

	allocate(bmat(bsize , bsize))
	allocate(rmat(bsize , bsize))

	bmat = breal + (0.0D0 , 1.0D0) * bimag
	cmat = creal + (0.0D0 , 1.0D0) * cimag
	rmat = bmat 
	do ii = 1 , bsize
		rmat(ii , ii) = rmat(ii , ii) + cmat
	end do

	call getenv("CLUSTER_BEGIN_TYPE" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	print * , "matrix"

	call getenv("CLUSTER_END_TYPE" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	call getenv("CLUSTER_BEGIN_DATA" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

	print * , bsize
	print * , dble(rmat)
	print * , dimag(rmat)

	call getenv("CLUSTER_END_DATA" , envvalue)
	write(* , "(A)" , advance = "no") trim(envvalue)

end program mm
